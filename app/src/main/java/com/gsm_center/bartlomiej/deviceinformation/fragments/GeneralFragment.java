package com.gsm_center.bartlomiej.deviceinformation.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.gsm_center.bartlomiej.deviceinformation.R;
import com.gsm_center.bartlomiej.deviceinformation.SetData;
import com.gsm_center.bartlomiej.deviceinformation.Tools;
import com.gsm_center.bartlomiej.deviceinformation.sections.GeneralInfo;

import java.util.Objects;

/*
    Created by Bartłomiej Poradzisz
 */

public class GeneralFragment extends Fragment
{

	boolean hasPermissions = false;

	public GeneralFragment()
	{

	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.fragment_general, container, false);
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	@Override
	public void onActivityCreated(Bundle savedInstanceBundle)
	{
		super.onActivityCreated(savedInstanceBundle);

		setAllGeneralData();
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	public void setAllGeneralData()
	{
		View v = getView();
		if (v != null)
		{
			checkPermissions();
			setBrand(v.findViewById(R.id.manufacturer));
			setDeviceName(v.findViewById(R.id.device));
			setSerialNumber(v.findViewById(R.id.serial));
			setDeviceId(v.findViewById(R.id.androidID));
			setSimSerial(v.findViewById(R.id.simSerial));
			setImei(v.findViewById(R.id.imei));
			setCountryOfOrigin(v);
			//        setScreenResolution((TextView) v.findViewById(R.id.resolution));
			//        setScreenSize((TextView) v.findViewById(R.id.screenSize));
			//        setScreenDensity((TextView) v.findViewById(R.id.density));
			setGsfId(v.findViewById(R.id.gsfId));
			setImsi(v.findViewById(R.id.imsi));
			setUUID(v.findViewById(R.id.uuid));
		}
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	private void setGsfId(TextView v)
	{
		SetData.setText(v, GeneralInfo.getGsfId(getContext()));
	}

	private void setImsi(TextView v)
	{
		//TODO detect sim if(isInserted) setText("Airplane mode is on")
		//todo else setText("NO SIM");
		SetData.setText(v,
				GeneralInfo.getDeviceImsi(Objects.requireNonNull(getTelephonyManager())));
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	private void checkPermissions()
	{
		int permissionCheck = ContextCompat.checkSelfPermission(
				Objects.requireNonNull(this.getContext()), Manifest.permission.READ_PHONE_STATE);
		hasPermissions = (permissionCheck == PackageManager.PERMISSION_GRANTED);
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	private void setCountryOfOrigin(View v)
	{
		if (Build.BRAND.toUpperCase().equals("SAMSUNG"))
		{
			TableLayout tableLayout = v.findViewById(R.id.tableGeneral);
			TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(0,
					ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
			String country = GeneralInfo.getCountryOfOrigin(
					GeneralInfo.getDeviceIMEI(getTelephonyManager(), hasPermissions),
					Build.BRAND.toUpperCase());

			tableLayout.addView(Tools.getTableRowWithTextViews(getContext(),
					Tools.getTextViewLabel(getContext(), layoutParams, R.id.country_of_origin_label,
							R.string.country_of_origin),
					Tools.getTextViewWithValue(getContext(), layoutParams, R.id.country_of_origin,
							country)), tableLayout.getChildCount(),
					new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
							ViewGroup.LayoutParams.WRAP_CONTENT));
		}
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	private void setUUID(TextView v)
	{
		SetData.setText(v, GeneralInfo
				.getDeviceUUID(getTelephonyManager(), hasPermissions, this.getContext()));
	}

	private void setImei(TextView v)
	{
		SetData.setText(v, GeneralInfo.getDeviceIMEI(getTelephonyManager(), hasPermissions));
	}

    /*
    private void setScreenSize(TextView v)
    {
        SetData.setText(v, .getScreenSize() + "\"");
    }

    private void setScreenResolution(TextView v)
    {
        SetData.setText(v, .getScreenResolution());
    }

    private void setScreenDensity(TextView v)
    {
        SetData.setText(v, .getScreenDensity() + " ppi");
    }*/

	private void setBrand(TextView v)
	{
		SetData.setText(v, Build.BRAND.toUpperCase());
	}

	private void setDeviceName(TextView v)
	{
		SetData.setText(v, Build.MODEL);
	}

	@SuppressLint("HardwareIds")
	private void setSerialNumber(TextView v)
	{
		SetData.setText(v, Build.SERIAL);
	}

	private void setDeviceId(TextView v)
	{
		SetData.setText(v, GeneralInfo.getDeviceID(Objects.requireNonNull(this.getContext())));
	}

	private void setSimSerial(TextView v)
	{
		SetData.setText(v, GeneralInfo.getSimSerial(getTelephonyManager()));
	}

	private TelephonyManager getTelephonyManager()
	{
		try
		{
			return (TelephonyManager) Objects.requireNonNull(getActivity()).getSystemService(
					Context.TELEPHONY_SERVICE);
		}
		catch (SecurityException e)
		{
			return null;
		}
	}

}
