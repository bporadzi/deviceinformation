package com.gsm_center.bartlomiej.deviceinformation.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gsm_center.bartlomiej.deviceinformation.R;
import com.gsm_center.bartlomiej.deviceinformation.SetData;
import com.gsm_center.bartlomiej.deviceinformation.Tools;
import com.gsm_center.bartlomiej.deviceinformation.sections.ConnectionInfo;

import java.util.Objects;

/*
    Created by Bartłomiej Poradzisz
 */

public class ConnectionFragment extends Fragment
{

	private final static int WIFI_CONNECTION = 1, MOBILE_CONNECTION = 0;

	public ConnectionFragment()
	{

	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.fragment_connection, container, false);
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);

		View v = getView();
		if (v != null)
		{
			setAllNetworkData(v);
		}
	}

	protected WifiManager getWifiManager()
	{
		return ((WifiManager) Objects.requireNonNull(this.getActivity()).getApplicationContext()
		                             .getSystemService(Context.WIFI_SERVICE));
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	public void setAllNetworkData(View v)
	{
		setIP(v.findViewById(R.id.ip));
		setMac(v.findViewById(R.id.mac));
		setBluetoothMac(v.findViewById(R.id.bluetoothMac));
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	private void setIP(TextView v)
	{
		if (Tools.checkWiFi(Objects.requireNonNull(this.getContext())) == WIFI_CONNECTION)
		{
			String mIP = (ConnectionInfo.getIP(getWifiManager()));
			SetData.setText(v, mIP);
		}
		else if (Tools.checkWiFi(this.getContext()) == MOBILE_CONNECTION)
		{
			SetData.setText(v, "Device is not connected to WiFi");
		}
		else
		{
			SetData.setText(v, "No internet connection");
		}
	}

	private void setMac(TextView v)
	{
		String mac = ConnectionInfo.getMacAddr();

		if (!mac.equals("Failed to resolve. Try to enable WiFi"))
		{
			mac = mac.toUpperCase();
		}

		SetData.setText(v, mac);
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	protected void setBluetoothMac(TextView v)
	{
		SetData.setMacAddress(v, ConnectionInfo.getBluetoothMAC(this.getContext()));
	}

	public BroadcastReceiver getConnectionReceiver()
	{
		return new BroadcastReceiver()
		{

			@RequiresApi(api = Build.VERSION_CODES.M)
			@Override
			public void onReceive(Context context, Intent intent)
			{
				View v = getView();
				if (v != null)
				{
					setIP(v.findViewById(R.id.ip));
					if (ConnectionInfo.hasWifiConnection(Objects.requireNonNull(getContext())))
					{
						setMac(v.findViewById(R.id.mac));
					}
				}
			}
		};

	}
}
