package com.gsm_center.bartlomiej.deviceinformation.fragments;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.gsm_center.bartlomiej.deviceinformation.R;
import com.gsm_center.bartlomiej.deviceinformation.SetData;
import com.gsm_center.bartlomiej.deviceinformation.sections.MemoryInfo;

import java.util.Objects;

public class MemoryFragment extends Fragment
{

	public Thread t;
	int totalRam;
	private View v;
	private Handler handler;

	public MemoryFragment()
	{
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.fragment_memory, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceBundle)
	{
		super.onActivityCreated(savedInstanceBundle);
		setMemoryData(getView());
	}

	@Override
	public void onPause()
	{
		if (handler != null)
		{
			handler.removeCallbacksAndMessages(null);
			handler = null;
		}
		if (t != null)
		{
			try
			{
				t.interrupt();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			t = null;
		}

		v = null;

		super.onPause();
	}

	@Override
	public void onResume()
	{
		super.onResume();
		v = getView();
		handler = new Handler();

		if (v != null)
		{
			t = new Thread(this::refreshRamUsage);
			t.start();
		}
	}

	public synchronized void refreshRamUsage()
	{
		while (true)
		{
			if (v != null && handler != null)
			{
				handler.post(this::showRefreshedData);
			}
			try
			{
				Thread.sleep(1500);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}


	@RequiresApi(api = Build.VERSION_CODES.M)
	protected void showRefreshedData()
	{
		int free = (int) MemoryInfo.getFreeRam(Objects.requireNonNull(getContext()));
		int busy = totalRam - free;

		int percentage = (int) (busy / (double) totalRam * 100);
		if (v != null)
		{
			TextView tvRamUsed = v.findViewById(R.id.ramUsed);
			TextView tvRamAvailable = v.findViewById(R.id.ramAvailable);
			setUsedRam(tvRamUsed, busy, percentage);
			setFreeRam(tvRamAvailable, free);

		}
	}

	protected void setMemoryData(View v)
	{
		setRamInfo(v);
		setStorageInfo(v);
	}

	private void setStorageInfo(View v)
	{
		setInternalStorage(v.findViewById(R.id.internalStorage));
		setAvailableInternalStorage(v.findViewById(R.id.availableStorage));
	}

	private void setAvailableInternalStorage(TextView v)
	{
		SetData.setText(v, MemoryInfo.availableMemoryWithPercentage());
	}

	private void setInternalStorage(TextView v)
	{
		SetData.setText(v, MemoryInfo.getInternalStorage() + "");
	}

	private void setRamInfo(View v)
	{
		totalRam = MemoryInfo.readTotalRam();

		setTotalRam(v.findViewById(R.id.totalRam), totalRam);
	}

	private void setFreeRam(TextView v, int free)
	{
		SetData.setText(v, free + " MB");
	}

	private void setTotalRam(TextView v, int value)
	{
		SetData.setText(v, value + " MB");
	}

	private void setUsedRam(TextView v, long value, int percentage)
	{
		SetData.setText(v, value + " MB " + "(" + percentage + "%)");
	}
}
