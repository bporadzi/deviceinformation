package com.gsm_center.bartlomiej.deviceinformation;

import android.Manifest;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.ImageButton;

import com.gsm_center.bartlomiej.deviceinformation.Dialogs.AppCompatMaterialAlertDialog;
import com.gsm_center.bartlomiej.deviceinformation.Dialogs.HardresetDialog;
import com.gsm_center.bartlomiej.deviceinformation.fragments.BatteryFragment;
import com.gsm_center.bartlomiej.deviceinformation.fragments.ConnectionFragment;
import com.gsm_center.bartlomiej.deviceinformation.fragments.CpuFragment;
import com.gsm_center.bartlomiej.deviceinformation.fragments.GeneralFragment;
import com.gsm_center.bartlomiej.deviceinformation.fragments.HardwareFragment;
import com.gsm_center.bartlomiej.deviceinformation.fragments.MemoryFragment;
import com.gsm_center.bartlomiej.deviceinformation.fragments.OsFragment;
import com.gsm_center.bartlomiej.deviceinformation.fragments.ResetFragment;
import com.gsm_center.bartlomiej.deviceinformation.sections.BatteryInfo;
import com.gsm_center.bartlomiej.deviceinformation.sections.CPUInfo;
import com.gsm_center.bartlomiej.deviceinformation.sections.CameraInf;
import com.gsm_center.bartlomiej.deviceinformation.sections.ConnectionInfo;
import com.gsm_center.bartlomiej.deviceinformation.sections.GeneralInfo;
import com.gsm_center.bartlomiej.deviceinformation.sections.MemoryInfo;
import com.gsm_center.bartlomiej.deviceinformation.sections.OsInfo;

import java.util.ArrayList;
import java.util.List;


/*
	Created by Bartłomiej Poradzisz
 */

public class MainActivity extends AppCompatActivity
{

	private final static int WIFI_CONNECTION = 1, REQUEST_READ_PHONE_STATE = 2,
			REQUEST_CODE_ENABLE_ADMIN = 10;
	private Toolbar toolbar;
	private TabLayout tabLayout;
	private ViewPager viewPager;
	private BroadcastReceiver batteryReceiver;
	private BroadcastReceiver connectionReceiver;
	private ViewPagerAdapter adapter;
	private Fragment currentFragment;
	private ImageButton infoButton, downloadButton;
	private DevicePolicyManager mDPM;

	private ImageButton.OnClickListener clickListener = v -> {
		switch (v.getId())
		{
			case R.id.buttonInfo:
			{
				try
				{
					startActivity(new Intent(MainActivity.this, AboutUsActivity.class));
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				break;
			}
			case R.id.buttonCopy:
			{
				new Thread(() -> {
					try
					{
						String separator = "------------------------\n";

						String allData = GeneralInfo.getGeneralData(getApplicationContext(),
								(TelephonyManager) getApplicationContext()
										.getSystemService(Context.TELEPHONY_SERVICE), true);
						allData += separator;
						allData += CPUInfo.getCpuData();
						allData += separator;
						allData += OsInfo.getOsData();
						allData += separator;
						allData += ConnectionInfo.getConnectionData(getApplicationContext(),
								(WifiManager) getApplicationContext()
										.getSystemService(Context.WIFI_SERVICE));
						allData += separator;
						allData += MemoryInfo.getMemoryData();
						allData += separator;
						allData += BatteryInfo.getBatteryData(getApplicationContext());
						allData += separator;
						allData += "Board:\t" + Build.BOARD + "\n" + "Hardware:\t" + Build.HARDWARE
								+ "\n";

						Intent sendIntent = new Intent();
						sendIntent.setAction(Intent.ACTION_SEND);
						sendIntent.putExtra(Intent.EXTRA_TEXT, allData);
						sendIntent.setType("text/plain");
						startActivity(sendIntent);
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
				}).start();
				break;
			}
			default:
			{
			}
		}
	};
	private ViewPager.OnPageChangeListener viewPagerListener = new ViewPager.OnPageChangeListener()
	{

		@Override
		public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
		{
		}

		@Override
		public void onPageSelected(int position)
		{
			if (currentFragment == null) return;
			currentFragment.onPause();

			currentFragment = adapter.getItem(position);
			if (currentFragment == null) return;
			switch (position)
			{
				case 1:
				{
					currentFragment.onResume();
					break;
				}
				case 4:
				{
					currentFragment.onResume();
					break;
				}
				case 5:
				{
					currentFragment.onResume();
					break;
				}
				case 6:
				{
					currentFragment.onResume();
					break;
				}
			}
		}

		@Override
		public void onPageScrollStateChanged(int state)
		{
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		requestPermissions();

		downloadHardwareInfo();
		checkWifi();
	}

	private void checkWifi()
	{
		final Context context = this.getApplicationContext();
		new Thread()
		{

			@Override
			public void run()
			{
				if (Tools.checkWiFi(context) != WIFI_CONNECTION)
				{
					askAboutWifi();
				}
			}
		}.run();
	}

	private void askAboutWifi()
	{
		SharedPreferences sharedPreferences = getSharedPreferences("values", 0);

		boolean neverAsk = sharedPreferences.getBoolean("never_ask", false);

		if (!neverAsk)
		{
			AppCompatMaterialAlertDialog dialog = AppCompatMaterialAlertDialog.getInstance();
			dialog.show(getFragmentManager(), "");
		}
		else if (sharedPreferences.getBoolean("enable_wifi_automatically", false))
		{
			Tools.enableWifi(this.getApplicationContext());
		}
	}

	@Override
	protected void onDestroy()
	{
		unregisterReceivers();

		try
		{
			if (downloadButton != null) downloadButton.setOnClickListener(null);
			if (infoButton != null) infoButton.setOnClickListener(null);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		infoButton = null;
		downloadButton = null;
		toolbar = null;
		tabLayout = null;
		viewPager = null;
		batteryReceiver = null;
		connectionReceiver = null;
		currentFragment = null;
		adapter = null;

		super.onDestroy();
	}

	public void reset(View v)
	{
		HardresetDialog dialog = HardresetDialog.getInstance();
		dialog.show(getFragmentManager(), "");
	}

	protected void unregisterReceivers()
	{
		try
		{
			unregisterReceiver(batteryReceiver);
			unregisterReceiver(connectionReceiver);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void downloadHardwareInfo()
	{
		new Thread(() -> {
			SharedPreferences sharedPreferences = getSharedPreferences("values", 0);
			boolean afterFirstLaunch = sharedPreferences.getBoolean("after_first_launch", false);

			if (afterFirstLaunch) return;

			SensorInfo sensorInfo = new SensorInfo(getApplicationContext());
			sensorInfo.gatherData();

			CameraInf cameraInfo = new CameraInf(getApplicationContext());
			cameraInfo.gatherData();
		}).start();
	}

	private void generateTabs()
	{
		toolbar = findViewById(R.id.toolbar);
		viewPager = findViewById(R.id.viewpager);
		tabLayout = findViewById(R.id.tabs);
		infoButton = findViewById(R.id.buttonInfo);
		downloadButton = findViewById(R.id.buttonCopy);

		infoButton.setOnClickListener(clickListener);
		downloadButton.setOnClickListener(clickListener);

		setSupportActionBar(toolbar);
		setupViewPager(viewPager);
		tabLayout.setupWithViewPager(viewPager);

	}

	private void setupViewPager(final ViewPager viewPager)
	{
		viewPager.setOffscreenPageLimit(0);
		adapter = new ViewPagerAdapter(getSupportFragmentManager());

		currentFragment = new GeneralFragment();
		adapter.addFragment(currentFragment, getString(R.string.general_tab));

		adapter.addFragment(new CpuFragment(), getString(R.string.cpu_tab));

		adapter.addFragment(new OsFragment(), getString(R.string.os_tab));
		ConnectionFragment connectionFragment = new ConnectionFragment();

		adapter.addFragment(connectionFragment, getString(R.string.connection_tab));

		adapter.addFragment(new MemoryFragment(), getString(R.string.memory_tab));

		BatteryFragment batteryFragment = new BatteryFragment();

		adapter.addFragment(batteryFragment, getString(R.string.battery_tab));

		batteryReceiver = batteryFragment.getBatteryReceiver();
		registerReceiver(batteryReceiver, getBatteryIntentFilter());

		connectionReceiver = connectionFragment.getConnectionReceiver();
		registerReceiver(connectionReceiver, getConnectionIntentFilter());

		adapter.addFragment(new HardwareFragment(), getString(R.string.hardware_tab));
		adapter.addFragment(new ResetFragment(), getString(R.string.reset_tab));

		viewPager.setAdapter(adapter);

		viewPager.addOnPageChangeListener(viewPagerListener);

		viewPager.post(() -> viewPagerListener.onPageSelected(0));
	}

	private IntentFilter getConnectionIntentFilter()
	{
		IntentFilter intentFilterConnection = new IntentFilter();
		intentFilterConnection.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
		return intentFilterConnection;
	}

	@RequiresApi(api = Build.VERSION_CODES.DONUT)
	private IntentFilter getBatteryIntentFilter()
	{
		IntentFilter intentFilterBattery = new IntentFilter();
		intentFilterBattery.addAction(Intent.ACTION_POWER_CONNECTED);
		intentFilterBattery.addAction(Intent.ACTION_POWER_DISCONNECTED);
		intentFilterBattery.addAction(Intent.ACTION_BATTERY_CHANGED);
		intentFilterBattery.addAction(Intent.ACTION_BATTERY_LOW);
		return intentFilterBattery;
	}

	protected void requestPermissions()
	{
		int permissionCheck = ContextCompat.checkSelfPermission(this,
				Manifest.permission.READ_PHONE_STATE);

		if (permissionCheck != PackageManager.PERMISSION_GRANTED)
		{
			ActivityCompat.requestPermissions(this,
					new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
		}
		else
		{
			generateTabs();
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
			@NonNull int[] grantResults)
	{
		if (requestCode == REQUEST_READ_PHONE_STATE)
		{
			generateTabs();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (requestCode == REQUEST_CODE_ENABLE_ADMIN)
		{
			if (resultCode == RESULT_OK)
			{
				try
				{
					mDPM = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
					if (mDPM == null) return;
					new Thread(() -> mDPM.wipeData(0)).start();
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
	}

	private class ViewPagerAdapter extends FragmentPagerAdapter
	{

		private final List <Fragment> mFragmentList = new ArrayList <>();
		private final List <String> mFragmentTitleList = new ArrayList <>();

		ViewPagerAdapter(android.support.v4.app.FragmentManager fm)
		{
			super(fm);
		}

		@Override
		public Fragment getItem(int position)
		{
			return mFragmentList.get(position);
		}

		@Override
		public int getCount()
		{
			return mFragmentList.size();
		}

		private void addFragment(Fragment fragment, String title)
		{
			mFragmentList.add(fragment);
			mFragmentTitleList.add(title);
		}

		@Override
		public CharSequence getPageTitle(int position)
		{
			return mFragmentTitleList.get(position);
		}
	}

}