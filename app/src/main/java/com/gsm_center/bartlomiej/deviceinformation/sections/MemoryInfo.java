package com.gsm_center.bartlomiej.deviceinformation.sections;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;

import com.gsm_center.bartlomiej.deviceinformation.Tools;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

import static android.content.Context.ACTIVITY_SERVICE;

public class MemoryInfo
{

	private static long totalMemory = -1;

	public static String getMemoryData()
	{
		String memoryData;

		memoryData = "Total RAM:\t" + readTotalRam() + "\n";
		memoryData += "Internal storage:\t" + getInternalStorage() + "\n";
		memoryData += "Available storage:\t" + availableMemoryWithPercentage() + "\n";

		return memoryData;
	}

	public static long getFreeRam(Context context)
	{
		ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
		ActivityManager activityManager = (ActivityManager) context.getSystemService(
				ACTIVITY_SERVICE);

		if (activityManager == null) return -1;
		activityManager.getMemoryInfo(mi);

		return mi.availMem / 1048576L;
	}

	public static synchronized int readTotalRam()
	{
		int tm = -1;
		try
		{
			RandomAccessFile reader = new RandomAccessFile("/proc/meminfo", "r");
			String load = reader.readLine();
			String[] totrm = load.split(" kB");
			String[] trm = totrm[0].split(" ");
			tm = Integer.parseInt(trm[trm.length - 1]);
			tm = Math.round(tm / 1024);
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return tm;
	}

	public static synchronized String getInternalStorage()
	{
		return (Build.VERSION.SDK_INT >= 18) ? newApiInternalStorage() : internalStorage();
	}

	@SuppressWarnings("deprecated")
	public static String availableMemoryWithPercentage()
	{
		try
		{
			File path = Environment.getDataDirectory();
			StatFs stat = new StatFs(path.getPath());
			long blockSize = stat.getBlockSize();
			long availableBlocks = stat.getAvailableBlocks();

			int percentage = (int) (availableBlocks * blockSize * 100 / totalMemory);
			return Tools.formatCapactiy(availableBlocks * blockSize) + " (" + percentage + "%)";
		}
		catch (Exception e)
		{
			return null;
		}
	}

    /*
    @TargetApi (18)
    private static String newApiAvailableMemoryWithPercentage()
    {
        try
        {
            File path = Environment.getDataDirectory();
            StatFs stat = new StatFs(path.getPath());
            long blockSize = stat.getBlockSizeLong();
            long availableBlocks = stat.getAvailableBlocksLong();

            int percentage = (int) (100 * availableBlocks * blockSize / totalMemory);

            return Tools.formatCapactiy(availableBlocks * blockSize) + " (" + percentage + "%)";
        } catch (Exception e)
        {
            return null;
        }
    }*/

	private static String internalStorage()
	{
		try
		{
			StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
			long blockSize = stat.getBlockSize();
			long totalBlocks = stat.getBlockCount();
			totalMemory = totalBlocks * blockSize;
			return Tools.formatCapactiy(totalBlocks * blockSize);
		}
		catch (Exception e)
		{
			return null;
		}
	}

	@TargetApi(18)
	private static String newApiInternalStorage()
	{
		try
		{
			StatFs stat = new StatFs(Environment.getDataDirectory().getPath());
			long blockSize = stat.getBlockSizeLong();
			long totalBlocks = stat.getBlockCountLong();
			totalMemory = totalBlocks * blockSize;
			return Tools.formatCapactiy(totalBlocks * blockSize);
		}
		catch (Exception e)
		{
			return null;
		}
	}
}
