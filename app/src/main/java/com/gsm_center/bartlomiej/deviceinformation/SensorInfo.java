package com.gsm_center.bartlomiej.deviceinformation;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;

import java.util.List;

import static android.content.Context.SENSOR_SERVICE;

/*
	Created by Bartłomiej on 18.07.2017.
 */

public class SensorInfo
{

	private Context context;

	SensorInfo(Context context)
	{
		this.context = context;

	}

	public void gatherData()
	{
		SharedPreferences sharedPreferences = context.getSharedPreferences("values",
				Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		List <Sensor> sensorList = detectSensors();

		editor.putBoolean("after_first_launch", true);

		editor.putBoolean("sensor_pressure", false);
		editor.putBoolean("gyroscope", false);
		editor.putBoolean("sensor_magnetic", false);
		editor.putBoolean("sensor_proximity", false);
		editor.putBoolean("sensor_accelerometer", false);
		editor.putBoolean("sensor_gravity", false);
		editor.putBoolean("sensor_magnetic_uncalibrated", false);
		editor.putBoolean("sensor_light", false);
		editor.putBoolean("rotation_vector", false);
		editor.putBoolean("game_rotation_vector", false);

		int numOfSensors = 0;

		assert sensorList != null;

		for (Sensor sensor : sensorList)
		{
			switch (sensor.getType())
			{
				case Sensor.TYPE_PRESSURE:
				{
					editor.putBoolean("sensor_pressure", true);
					numOfSensors++;
					break;
				}
				case Sensor.TYPE_GYROSCOPE:
				{
					editor.putBoolean("gyroscope", true);
					numOfSensors++;
					break;
				}
				case Sensor.TYPE_MAGNETIC_FIELD:
				{
					editor.putBoolean("sensor_magnetic", true);
					numOfSensors++;
					break;
				}
				case Sensor.TYPE_AMBIENT_TEMPERATURE:
				{
					editor.putBoolean("thermometer", true);
					numOfSensors++;
					break;
				}
				case Sensor.TYPE_RELATIVE_HUMIDITY:
				{
					editor.putBoolean("sensor_humidity", true);
					numOfSensors++;
					break;
				}
				case Sensor.TYPE_PROXIMITY:
				{
					editor.putBoolean("sensor_proximity", true);
					numOfSensors++;
					break;
				}
				case Sensor.TYPE_ACCELEROMETER:
				{
					editor.putBoolean("sensor_accelerometer", true);
					numOfSensors++;
					break;
				}
				case Sensor.TYPE_GRAVITY:
				{
					editor.putBoolean("sensor_gravity", true);
					numOfSensors++;
					break;
				}
				case Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED:
				{
					editor.putBoolean("sensor_magnetic_uncalibrated", true);
					numOfSensors++;
					break;
				}
				case Sensor.TYPE_LIGHT:
				{
					editor.putBoolean("sensor_light", true);
					numOfSensors++;
					break;
				}
				case Sensor.TYPE_ROTATION_VECTOR:
				{
					editor.putBoolean("rotation_vector", true);
					numOfSensors++;
					break;
				}
				case Sensor.TYPE_GAME_ROTATION_VECTOR:
				{
					editor.putBoolean("game_rotation_vector", true);
					numOfSensors++;
					break;
				}
			}
		}
		editor.putInt("number_of_sensors", numOfSensors);
		editor.apply();
	}

	private List <Sensor> detectSensors()
	{
		SensorManager sensorManager = (SensorManager) context.getSystemService(SENSOR_SERVICE);

		if (sensorManager == null) return null;
		return sensorManager.getSensorList(Sensor.TYPE_ALL);
	}
}
