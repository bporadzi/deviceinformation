package com.gsm_center.bartlomiej.deviceinformation.fragments;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.text.style.SuperscriptSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.gsm_center.bartlomiej.deviceinformation.R;
import com.gsm_center.bartlomiej.deviceinformation.SetData;
import com.gsm_center.bartlomiej.deviceinformation.Tools;

import java.util.Objects;

import static android.content.Context.SENSOR_SERVICE;

public class HardwareFragment extends Fragment
{

	Sensor[] supportedSensors;
	private SensorManager sensorManager;
	private View v;

	private Handler handler;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.fragment_hardware, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceBundle)
	{
		super.onActivityCreated(savedInstanceBundle);

		v = getView();
		handler = new Handler();
		new Thread(this::showData).start();
		new Thread(this::addDataRows).start();
	}

	@Override
	public void onResume()
	{
		super.onResume();
		if (v == null) v = getView();
	}

	@Override
	public void onPause()
	{
		v = null;
		new Thread(() -> {
			try
			{
				if (handler != null) handler.removeCallbacksAndMessages(null);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}).start();
		super.onPause();
	}

	@Override
	public void onDestroy()
	{
		if (sensorManager != null)
		{
			sensorManager.unregisterListener(sensorListener);
			sensorManager = null;
		}

		if (supportedSensors != null)
		{
			supportedSensors = null;
		}

		v = null;

		super.onDestroy();
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	private void showData()
	{
		new Thread(() -> showCameraInfo(getContext())).start();
		showHardwareInfo();
	}

	@TargetApi(21)
	private void showHardwareInfo()
	{
		setBoard(v.findViewById(R.id.board));
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	private void showCameraInfo(Context context)
	{
		if (context == null) return;
		if (checkCameraHardware(context))
		{
			SharedPreferences sharedPreferences = context.getSharedPreferences("values",
					Context.MODE_PRIVATE);

			int numberOfCameras = sharedPreferences.getInt("number_of_cameras", 0);

			for (int i = 0; i < numberOfCameras; i++)
			{
				TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(0,
						ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);

				int index = numberOfCameras - i - 1;
				String key = "camera" + index;
				float val = sharedPreferences.getFloat(key, 0);

				final TableRow tableRow = Tools.getTableRowWithTextViews(getContext(),
						Tools.getTextViewLabel(getContext(), layoutParams,
								getResources().getString(R.string.camera) + " " + (numberOfCameras
										- i) + ":"),
						Tools.getTextViewWithValue(getContext(), layoutParams, R.id.camera,
								val + " MP"));
				try
				{
					if (handler != null)
					{
						handler.post(() -> {
							if (v != null)
							{
								TableLayout tableLayout = v.findViewById(R.id.tableHardware);
								try
								{
									tableLayout.addView(tableRow, 1, new ViewGroup.LayoutParams(
											ViewGroup.LayoutParams.MATCH_PARENT,
											ViewGroup.LayoutParams.WRAP_CONTENT));
								}
								catch (Exception e)
								{
									try
									{
										tableLayout.addView(tableRow, 0, new ViewGroup.LayoutParams(
												ViewGroup.LayoutParams.MATCH_PARENT,
												ViewGroup.LayoutParams.WRAP_CONTENT));
									}
									catch (Exception ex)
									{
										ex.printStackTrace();
									}
								}
							}
						});
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
	}

	private boolean checkCameraHardware(Context context)
	{
		return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
	}

	private void updateSensorData(final int textViewToChange, final String value)
	{
		if (v != null)
		{
			TextView tv = v.findViewById(textViewToChange);
			SetData.setText(tv, value);
		}
	}

	private void updateSensorData(final int textViewToChange, final SpannableStringBuilder value)
	{
		if (v != null)
		{
			TextView tv = v.findViewById(textViewToChange);
			if (tv != null) SetData.setText(tv, value);
		}
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	private void addSensor(final TableLayout tableLayout, final int labelId, final int id,
			final int labelText)
	{
		final TableRow.LayoutParams textViewParams = new TableRow.LayoutParams(0,
				ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);

		if (handler != null)
		{
			handler.post(() -> tableLayout.addView(Tools.getTableRowWithTextViews(getContext(),
					Tools.getTextViewLabel(getContext(), textViewParams, labelId, labelText),
					Tools.getTextViewWithValue(getContext(), textViewParams, id,
							R.string.monitoring_default_value)), tableLayout.getChildCount(),
					new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
							ViewGroup.LayoutParams.WRAP_CONTENT)));
		}
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	private void addSensor(final TableLayout tableLayout, int labelText, int labelX, int labelY,
			int labelZ)
	{
		TableRow.LayoutParams rowParams = new TableRow.LayoutParams(
				ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);

		TableRow.LayoutParams textViewParams = new TableRow.LayoutParams(0,
				ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);

		for (int i = 0; i < 4; i++)
		{
			final TableRow tableRow = new TableRow(getContext());
			tableRow.setLayoutParams(rowParams);
			tableRow.setGravity(Gravity.CENTER);

			if (i == 0)
			{
				tableRow.addView(Tools.getTextViewLabel(getContext(), textViewParams, labelText));
			}
			else
			{
				int id;
				switch (i)
				{
					case 1:
					{
						labelText = R.string.surface_x;
						id = labelX;
						break;
					}
					case 2:
					{
						labelText = R.string.surface_y;
						id = labelY;
						break;
					}
					case 3:
					{
						labelText = R.string.surface_z;
						id = labelZ;
						break;
					}
					default:
					{
						id = -1;
					}
				}

				tableRow.addView(Tools.getTextViewLabel(getContext(), textViewParams, labelText));
				tableRow.addView(Tools.getTextViewLabel(getContext(), textViewParams, id,
						R.string.monitoring_default_value));

			}

			if (handler != null)
			{
				handler.post(() -> tableLayout.addView(tableRow, tableLayout.getChildCount(),
						new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
								ViewGroup.LayoutParams.WRAP_CONTENT)));
			}
		}
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	private void addDataRows()
	{
		if (v != null)
		{
			SharedPreferences sharedPreferences = Objects.requireNonNull(getContext())
			                                             .getSharedPreferences("values",
					                                             Context.MODE_PRIVATE);

			supportedSensors = new Sensor[sharedPreferences.getInt("number_of_sensors", 0)];
			final TableLayout tableLayout = v.findViewById(R.id.tableHardware);

			sensorManager = (SensorManager) Objects.requireNonNull(getActivity()).getSystemService(
					SENSOR_SERVICE);

			boolean hasPressureSensor = sharedPreferences.getBoolean("sensor_pressure", false);
			boolean hasGyroscope = sharedPreferences.getBoolean("gyroscope", false);
			boolean hasMagneticSensor = sharedPreferences.getBoolean("sensor_magnetic", false);
			boolean hasProximitySensor = sharedPreferences.getBoolean("sensor_proximity", false);
			boolean hasAccelerometer = sharedPreferences.getBoolean("sensor_accelerometer", false);
			boolean hasGravitySensor = sharedPreferences.getBoolean("sensor_gravity", false);
			boolean hasMagneticUncalibratedSensor = sharedPreferences.getBoolean(
					"sensor_magnetic_uncalibrated", false);
			boolean hasLightSensor = sharedPreferences.getBoolean("sensor_light", false);
			boolean hasRotationVector = sharedPreferences.getBoolean("rotation_vector", false);
			boolean hasGameRotationVector = sharedPreferences.getBoolean("game_rotation_vector",
					false);
			boolean hasThermometer = sharedPreferences.getBoolean("thermometer", false);
			boolean hasHumiditySensor = sharedPreferences.getBoolean("sensor_humidity", false);

			int counter = 0;

			SensorManager sensorManager = ((SensorManager) getActivity().getSystemService(
					SENSOR_SERVICE));

			if (sensorManager == null) return;

			if (hasProximitySensor)
			{
				Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
				if (sensor != null)
				{
					supportedSensors[counter++] = sensor;
					addSensor(tableLayout, R.id.proximity_sensor_label, R.id.proximity_sensor,
							R.string.proximity_sensor);
				}
			}

			if (hasLightSensor)
			{
				Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
				if (sensor != null)
				{
					supportedSensors[counter++] = sensor;
					addSensor(tableLayout, R.id.light_sensor_label, R.id.light_sensor,
							R.string.light_sensor);
				}
			}

			if (hasThermometer)
			{
				Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
				if (sensor != null)
				{
					supportedSensors[counter++] = sensor;
					addSensor(tableLayout, R.id.thermometer_label, R.id.thermometer,
							R.string.thermometer);
				}
			}

			if (hasPressureSensor)
			{
				Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
				if (sensor != null)
				{
					supportedSensors[counter++] = sensor;
					addSensor(tableLayout, R.id.pressure_sensor_label, R.id.pressure_sensor,
							R.string.barometer);
				}
			}

			if (hasHumiditySensor)
			{
				Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
				if (sensor != null)
				{
					supportedSensors[counter++] = sensor;
					addSensor(tableLayout, R.id.humidity_sensor_label, R.id.humidity_sensor,
							R.string.humidity_sensor);
				}
			}

			if (hasGyroscope)
			{
				Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
				if (sensor != null)
				{
					supportedSensors[counter++] = sensor;
					addSensor(tableLayout, R.string.gyroscope, R.id.gyroscope_x, R.id.gyroscope_y,
							R.id.gyroscope_z);
				}
			}
			if (hasMagneticSensor)
			{
				Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

				if (sensor != null)
				{
					supportedSensors[counter++] = sensor;
					addSensor(tableLayout, R.string.magnetic, R.id.magnetic_x, R.id.magnetic_y,
							R.id.magnetic_z);
				}
			}
			if (hasAccelerometer)
			{
				Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
				if (sensor != null)
				{
					supportedSensors[counter++] = sensor;
					addSensor(tableLayout, R.string.accelerometer, R.id.accelerometer_x,
							R.id.accelerometer_y, R.id.accelerometer_z);
				}
			}
			if (hasGravitySensor)
			{
				Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
				if (sensor != null)
				{
					supportedSensors[counter++] = sensor;
					addSensor(tableLayout, R.string.gravitySensor, R.id.gravity_sensor_x,
							R.id.gravity_sensor_y, R.id.gravity_sensor_z);
				}
			}
			if (hasMagneticUncalibratedSensor)
			{
				if (Build.VERSION.SDK_INT >= 18)
				{
					Sensor sensor = sensorManager.getDefaultSensor(
							Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED);
					if (sensor != null)
					{
						supportedSensors[counter++] = sensor;
						addSensor(tableLayout, R.string.magnetic_uncalibrated, R.id.accelerometer_x,
								R.id.accelerometer_y, R.id.accelerometer_z);
					}
				}
			}
			if (hasRotationVector)
			{
				Sensor sensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
				if (sensor != null)
				{
					supportedSensors[counter++] = sensor;
					addSensor(tableLayout, R.string.rotation_vector, R.id.rotation_x,
							R.id.rotation_y, R.id.rotation_z);
				}
			}

			if (hasGameRotationVector)
			{
				@SuppressLint("InlinedApi") Sensor sensor = sensorManager.getDefaultSensor(
						Sensor.TYPE_GAME_ROTATION_VECTOR);
				if (sensor != null)
				{
					supportedSensors[counter++] = sensor;
					addSensor(tableLayout, R.string.game_rotation_vector, R.id.game_rotation_x,
							R.id.game_rotation_y, R.id.game_rotation_z);
				}
			}

			registerListeners();
		}
	}

	private void registerListeners()
	{
		if (supportedSensors != null)
		{
			for (Sensor sensor : supportedSensors)
			{
				if (sensor == null) continue;
				switch (sensor.getType())
				{
					case Sensor.TYPE_ACCELEROMETER:
					{
						if (sensorManager != null)
						{
							sensorManager.registerListener(sensorListener, sensor,
									SensorManager.SENSOR_DELAY_NORMAL);
						}

						break;
					}
					case Sensor.TYPE_PRESSURE:
					{
						if (sensorManager != null)
						{
							sensorManager.registerListener(sensorListener, sensor,
									SensorManager.SENSOR_DELAY_UI);
						}

						break;
					}
					case Sensor.TYPE_GYROSCOPE:
					{
						if (sensorManager != null)
						{
							sensorManager.registerListener(sensorListener, sensor,
									SensorManager.SENSOR_DELAY_NORMAL);
						}

						break;
					}
					case Sensor.TYPE_MAGNETIC_FIELD:
					{
						if (sensorManager != null)
						{
							sensorManager.registerListener(sensorListener, sensor,
									SensorManager.SENSOR_DELAY_NORMAL);
						}

						break;
					}
					case Sensor.TYPE_PROXIMITY:
					{
						if (sensorManager != null)
						{
							sensorManager.registerListener(sensorListener, sensor,
									SensorManager.SENSOR_DELAY_NORMAL);
							updateSensorData(R.id.proximity_sensor,
									sensor.getMaximumRange() + " cm");
						}

						break;
					}
					case Sensor.TYPE_GRAVITY:
					{
						sensorManager.registerListener(sensorListener, sensor,
								SensorManager.SENSOR_DELAY_NORMAL);

						break;
					}
					case Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED:
					{
						sensorManager.registerListener(sensorListener, sensor,
								SensorManager.SENSOR_DELAY_NORMAL);

						break;
					}
					case Sensor.TYPE_LIGHT:
					{
						if (sensorManager != null)
						{
							sensorManager.registerListener(sensorListener, sensor,
									SensorManager.SENSOR_DELAY_UI);
						}

						break;
					}
					case Sensor.TYPE_ROTATION_VECTOR:
					{
						if (sensorManager != null)
						{
							sensorManager.registerListener(sensorListener, sensor,
									SensorManager.SENSOR_DELAY_NORMAL);
						}

						break;
					}
					case Sensor.TYPE_GAME_ROTATION_VECTOR:
					{
						if (sensorManager != null)
						{
							sensorManager.registerListener(sensorListener, sensor,
									SensorManager.SENSOR_DELAY_NORMAL);
						}

						break;
					}
					case Sensor.TYPE_AMBIENT_TEMPERATURE:
					{
						if (sensorManager != null)
						{
							sensorManager.registerListener(sensorListener, sensor,
									SensorManager.SENSOR_DELAY_NORMAL);
						}
						break;
					}
					case Sensor.TYPE_RELATIVE_HUMIDITY:
					{
						if (sensorManager != null)
						{
							sensorManager.registerListener(sensorListener, sensor,
									SensorManager.SENSOR_DELAY_NORMAL);
						}
						break;
					}
				}
			}
		}
	}

	private void setBoard(TextView v)
	{
		SetData.setText(v, Build.BOARD);
	}

	private SensorEventListener sensorListener = new SensorEventListener()
	{

		@Override
		public void onSensorChanged(SensorEvent sensorEvent)
		{
			if (v == null || sensorEvent == null || sensorEvent.sensor == null) return;
			switch (sensorEvent.sensor.getType())
			{
				case Sensor.TYPE_ACCELEROMETER:
				{
					String values[] = new String[3];

					for (int i = 0; i < 3; i++)
					{
						values[i] = Tools.round(sensorEvent.values[i], 3) + " m/s2";

						SpannableStringBuilder cs = new SpannableStringBuilder(values[i]);
						cs.setSpan(new SuperscriptSpan(), values[i].length() - 1,
								values[i].length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
						cs.setSpan(new RelativeSizeSpan(0.75f), values[i].length() - 1,
								values[i].length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

						switch (i)
						{
							case 0:
							{
								updateSensorData(R.id.accelerometer_x, cs);
								break;
							}
							case 1:
							{
								updateSensorData(R.id.accelerometer_y, cs);
								break;
							}
							case 2:
							{
								updateSensorData(R.id.accelerometer_z, cs);
								break;
							}
						}
					}
					break;
				}
				case Sensor.TYPE_GYROSCOPE:
				{
					double data[] = new double[3];
					data[0] = Tools.round(sensorEvent.values[0], 3);
					data[1] = Tools.round(sensorEvent.values[1], 3);
					data[2] = Tools.round(sensorEvent.values[2], 3);

					updateSensorData(R.id.gyroscope_x, data[0] + "");
					updateSensorData(R.id.gyroscope_y, data[1] + "");
					updateSensorData(R.id.gyroscope_z, data[2] + "");
					break;
				}
				case Sensor.TYPE_MAGNETIC_FIELD:
				{
					double data[] = new double[3];
					data[0] = Tools.round(sensorEvent.values[0], 3);
					data[1] = Tools.round(sensorEvent.values[1], 3);
					data[2] = Tools.round(sensorEvent.values[2], 3);

					updateSensorData(R.id.magnetic_x, data[0] + " \u00B5T");
					updateSensorData(R.id.magnetic_y, data[1] + " \u00B5T");
					updateSensorData(R.id.magnetic_z, data[2] + " \u00B5T");

					break;
				}
				case Sensor.TYPE_PRESSURE:
				{
					updateSensorData(R.id.pressure_sensor, sensorEvent.values[0] + " hPa");
					break;
				}
				case Sensor.TYPE_GRAVITY:
				{
					String values[] = new String[3];

					for (int i = 0; i < 3; i++)
					{
						values[i] = Tools.round(sensorEvent.values[i], 3) + " m/s2";

						SpannableStringBuilder cs = new SpannableStringBuilder(values[i]);
						cs.setSpan(new SuperscriptSpan(), values[i].length() - 1,
								values[i].length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
						cs.setSpan(new RelativeSizeSpan(0.75f), values[i].length() - 1,
								values[i].length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

						switch (i)
						{
							case 0:
							{
								updateSensorData(R.id.gravity_sensor_x, cs);
								break;
							}
							case 1:
							{
								updateSensorData(R.id.gravity_sensor_y, cs);
								break;
							}
							case 2:
							{
								updateSensorData(R.id.gravity_sensor_z, cs);
								break;
							}
						}
					}
					break;
				}
				case Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED:
				{
					double data[] = new double[3];
					data[0] = Tools.round(sensorEvent.values[0], 3);
					data[1] = Tools.round(sensorEvent.values[1], 3);
					data[2] = Tools.round(sensorEvent.values[2], 3);

					updateSensorData(R.id.magnetic_uncalibrated_x, data[0] + " \u00B5T");
					updateSensorData(R.id.magnetic_uncalibrated_y, data[1] + " \u00B5T");
					updateSensorData(R.id.magnetic_uncalibrated_z, data[2] + " \u00B5T");
					break;
				}
				case Sensor.TYPE_ROTATION_VECTOR:
				{
					double data[] = new double[3];
					data[0] = Tools.round(sensorEvent.values[0], 3);
					data[1] = Tools.round(sensorEvent.values[1], 3);
					data[2] = Tools.round(sensorEvent.values[2], 3);

					updateSensorData(R.id.rotation_x, data[0] + "");
					updateSensorData(R.id.rotation_y, data[1] + "");
					updateSensorData(R.id.rotation_z, data[2] + "");
					break;
				}
				case Sensor.TYPE_GAME_ROTATION_VECTOR:
				{
					double data[] = new double[3];
					data[0] = Tools.round(sensorEvent.values[0], 3);
					data[1] = Tools.round(sensorEvent.values[1], 3);
					data[2] = Tools.round(sensorEvent.values[2], 3);

					updateSensorData(R.id.game_rotation_x, data[0] + "");
					updateSensorData(R.id.game_rotation_y, data[1] + "");
					updateSensorData(R.id.game_rotation_z, data[2] + "");
					break;
				}
				case Sensor.TYPE_LIGHT:
				{
					updateSensorData(R.id.light_sensor, sensorEvent.values[0] + " lux");
					break;
				}
				case Sensor.TYPE_PROXIMITY:
				{
					updateSensorData(R.id.proximity_sensor, sensorEvent.values[0] + " cm");
					break;
				}
				case Sensor.TYPE_AMBIENT_TEMPERATURE:
				{
					updateSensorData(R.id.thermometer, sensorEvent.values[0] + " \u2103");
					break;
				}
				case Sensor.TYPE_RELATIVE_HUMIDITY:
				{
					updateSensorData(R.id.humidity_sensor, sensorEvent.values[0] + " %");
					break;
				}
			}
		}

		@Override
		public void onAccuracyChanged(Sensor sensor, int i)
		{

		}
	};

}
