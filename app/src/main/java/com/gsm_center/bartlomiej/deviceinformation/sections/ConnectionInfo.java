package com.gsm_center.bartlomiej.deviceinformation.sections;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

import com.gsm_center.bartlomiej.deviceinformation.Tools;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

/*
	Created by Bartłomiej Poradzisz
 */

public class ConnectionInfo
{

    public static String getConnectionData(Context context, WifiManager wifiManager)
    {
        String connectionData;
        connectionData = "IP:\t" + getIP(wifiManager) + "\n";
        connectionData += "MAC:\t" + getMacAddr() + "\n";
        connectionData += "Bluetooth MAC:\t" + getBluetoothMAC(context) + "\n";
        return connectionData;
    }

    public static boolean hasWifiConnection(Context context)
    {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null) return false;
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo)
        {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected()) return true;
        }
        return false;
    }

    public static String getMacAddr()
    {
        try
        {
            List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface nif : all)
            {
                if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

                byte[] macBytes = nif.getHardwareAddress();
                if (macBytes == null)
                {
                    return "";
                }

                StringBuilder res1 = new StringBuilder();
                for (byte b : macBytes)
                {
                    if (b == 0)
                    {
                        res1.append("00:");
                    } else
                    {
                        res1.append(Integer.toHexString(b & 0xFF));
                        res1.append(":");
                    }
                }

                if (res1.length() > 0)
                {
                    res1.deleteCharAt(res1.length() - 1);
                }
                return res1.toString();
            }
        } catch (Exception ex)
        {
            return ex.toString();
        }
        return "Failed to resolve. Try to enable WiFi";
    }

    public static String getIP(WifiManager wm)
    {
        try
        {
            byte[] ipAddress = BigInteger.valueOf(wm.getConnectionInfo().getIpAddress()).toByteArray();

            ipAddress = Tools.reverseArray(ipAddress);
            InetAddress myAddress = InetAddress.getByAddress(ipAddress);

            return myAddress.getHostAddress();
        } catch (Exception e)
        {
            return "Error occurred";
        }
    }

    public static String getBluetoothMAC(Context context)
    {
        try
        {
            return android.provider.Settings.Secure.getString(context.getContentResolver(), "bluetooth_address");
        } catch (Exception e)
        {
            return "Bluetooth card not detected";
        }
    }

}
