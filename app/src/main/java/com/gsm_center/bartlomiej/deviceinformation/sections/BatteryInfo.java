package com.gsm_center.bartlomiej.deviceinformation.sections;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

/*
    Created by Bartłomiej Poradzisz
 */

public class BatteryInfo
{
    private static int status;

    public static String getBatteryData(Context context)
    {
        String batteryData;

        batteryData = "Battery condition:\t" + getBatteryHealth(context) + "\n";
        batteryData += "Battery status:\t" + getBatteryStatus(context) + "\n";
        batteryData += "Battery level:\t" + getBatteryLevel(context) + "\n";
        batteryData += "Battery technology:\t" + getBatteryTechnology(context) + "\n";
        batteryData += "Battery voltage:\t" + getBatteryVoltage(context) + "\n";

        return batteryData;
    }

    public static String getBatteryHealth(Context context)
    {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);


        if (batteryStatus != null)
        {
            status = batteryStatus.getIntExtra(BatteryManager.EXTRA_HEALTH, 0);

            switch (status)
            {
                case BatteryManager.BATTERY_HEALTH_GOOD:
                {
                    return "Good";
                }
                case BatteryManager.BATTERY_HEALTH_COLD:
                {
                    return "Cold";
                }
                case BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE:
                {
                    return "Over voltage";
                }
                case BatteryManager.BATTERY_HEALTH_OVERHEAT:
                {
                    return "Overheat";
                }
                case BatteryManager.BATTERY_HEALTH_UNKNOWN:
                {
                    return "Unknown";
                }
                case BatteryManager.BATTERY_HEALTH_DEAD:
                {
                    return "Dead";
                }
                case BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE:
                {
                    return "Unspecified failure";
                }
                default:
                {
                    return "Error";
                }
            }
        } else
        {
            return "Error";
        }
    }

    public static String getBatteryVoltage(Context context)
    {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);

        return (batteryStatus != null) ? batteryStatus.getIntExtra(BatteryManager.EXTRA_VOLTAGE, 0) + " mV" : null;
    }

    public static int getBatteryLevel(Context context)
    {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);

        return (batteryStatus != null) ? batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) : -1;
    }

    public static String getBatteryStatus(Context context)
    {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);

        if (batteryStatus != null)
        {
            status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);

            int chargePlug = batteryStatus.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
            boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
            boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;

            switch (status)
            {
                case BatteryManager.BATTERY_STATUS_CHARGING:
                {
                    if (usbCharge)
                    {
                        return "Charging (USB)";
                    } else if (acCharge)
                    {
                        return "Charging (AC)";
                    } else
                    {
                        return "Charging";
                    }
                }
                case BatteryManager.BATTERY_STATUS_DISCHARGING:
                {
                    return "Discharging";
                }
                case BatteryManager.BATTERY_STATUS_FULL:
                {
                    return "Full";
                }
                case BatteryManager.BATTERY_STATUS_NOT_CHARGING:
                {
                    return "Not charging";
                }
                case BatteryManager.BATTERY_STATUS_UNKNOWN:
                {
                    return "Unknown";
                }
                default:
                {
                    return null;
                }
            }
        } else
        {
            return null;
        }
    }

    public static String getBatteryTechnology(Context context)
    {
        IntentFilter intentfilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, intentfilter);

        return batteryStatus != null ? batteryStatus.getStringExtra("technology") : "Failed to resolve";
    }
}
