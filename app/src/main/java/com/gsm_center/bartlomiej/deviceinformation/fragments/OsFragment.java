package com.gsm_center.bartlomiej.deviceinformation.fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.gsm_center.bartlomiej.deviceinformation.R;
import com.gsm_center.bartlomiej.deviceinformation.SetData;
import com.gsm_center.bartlomiej.deviceinformation.Tools;
import com.gsm_center.bartlomiej.deviceinformation.sections.OsInfo;

/*
    Created by Bartłomiej Poradzisz
 */

public class OsFragment extends Fragment
{

	public OsFragment()
	{

	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.fragment_os, container, false);
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);

		View v = getView();
		if (v != null)
		{
			showSecurityPatch(v);
			showSamsungSerialNumber(v);
			showSamsungCSC(v);
			showBootloaderVersion(v);
			showRadioVersion(v);
		}
	}

	@Override
	public void onResume()
	{
		super.onResume();

		View v = getView();
		if (v != null)
		{
			showData(v);
		}
	}

	@Override
	public void onPause()
	{
		super.onPause();
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	private void showSamsungCSC(View v)
	{
		if (Build.BRAND.toUpperCase().equals("SAMSUNG"))
		{
			String code = OsInfo.getSamsungCSC();
			//            System.out.println(code);
			if (code != null)
			{
				TableRow.LayoutParams textViewParams = new TableRow.LayoutParams(
						TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

				TableLayout tableLayout = v.findViewById(R.id.tableOs);
				tableLayout.addView(Tools.getTableRowWithTextViews(getContext(),
						Tools.getTextViewLabel(getContext(), textViewParams, R.id.samsung_csc_label,
								R.string.samsung_csc),
						Tools.getTextViewWithValue(getContext(), textViewParams, R.id.samsung_csc,
								code.trim().toUpperCase())), tableLayout.getChildCount(),
						new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
								ViewGroup.LayoutParams.WRAP_CONTENT));
			}
		}
	}

	private void showData(View v)
	{
		showAndroidVersion(v.findViewById(R.id.androidVersion));
		showAndroidSDK(v.findViewById(R.id.androidSDK));

		showFingerPrint(v.findViewById(R.id.fingerprint));
		showCodenameVersion(v.findViewById(R.id.codename));

		showBuildId(v.findViewById(R.id.buildId));
		showKernelVersion(v.findViewById(R.id.kernelVersion));
		showKernelBuildDate(v.findViewById(R.id.softwareReleaseDate));

		String jvmVersion = OsInfo.getJavaVersion();

		showJavaVersion(v.findViewById(R.id.javaVersion), jvmVersion);
		showJavaRuntime(v.findViewById(R.id.androidRuntime), jvmVersion);

		showRootAccess(v.findViewById(R.id.root));
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	private void showSamsungSerialNumber(View v)
	{
		if (Build.BRAND.toUpperCase().equals("SAMSUNG"))
		{
			String serialNumber = OsInfo.getSamsungSerialNumber();
			if (serialNumber != null && !serialNumber.equals("00000000000"))
			{
				TableRow.LayoutParams textViewParams = new TableRow.LayoutParams(
						TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

				TableLayout tableLayout = v.findViewById(R.id.tableOs);
				tableLayout.addView(Tools.getTableRowWithTextViews(getContext(),
						Tools.getTextViewLabel(getContext(), textViewParams,
								R.id.samsung_serial_label, R.string.samsung_serial),
						Tools.getTextViewWithValue(getContext(), textViewParams,
								R.id.samsung_serial, serialNumber)), tableLayout.getChildCount(),
						new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
								ViewGroup.LayoutParams.WRAP_CONTENT));
			}
		}
	}

	private void showRootAccess(TextView v)
	{
		SetData.setText(v, OsInfo.isRooted() ? "Yes" : "No");
	}

	private void showJavaVersion(TextView v, String version)
	{
		SetData.setText(v, version);
	}

	private void showJavaRuntime(TextView v, String version)
	{
		SetData.setText(v, OsInfo.getJavaRuntime(version));

	}

	private void showKernelBuildDate(TextView v)
	{
		SetData.setText(v, OsInfo.getKernelBuildDate());
	}

	private void showKernelVersion(TextView v)
	{
		SetData.setText(v, OsInfo.getKernelInfo());
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	private void showSecurityPatch(View v)
	{
		String securityPatch = OsInfo.getSecurityPatch();
		if (!securityPatch.startsWith("Not available for"))
		{
			TableRow.LayoutParams textViewParams = new TableRow.LayoutParams(0,
					ViewGroup.LayoutParams.WRAP_CONTENT, 1f);


			TableLayout tableLayout = v.findViewById(R.id.tableOs);

			tableLayout.addView(Tools.getTableRowWithTextViews(getContext(),
					Tools.getTextViewLabel(getContext(), textViewParams, R.id.security_patch_label,
							R.string.security_patch),
					Tools.getTextViewWithValue(getContext(), textViewParams, R.id.security_patch,
							securityPatch)), tableLayout.getChildCount(),
					new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
							ViewGroup.LayoutParams.WRAP_CONTENT));
		}
	}

	private void showCodenameVersion(TextView v)
	{
		SetData.setText(v, OsInfo.getCodename(Build.VERSION.SDK_INT + ""));
	}

	private void showBuildId(TextView v)
	{
		SetData.setText(v, Build.DISPLAY);
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	private void showRadioVersion(View v)
	{
		String version = Build.getRadioVersion();

		if (!version.equals(Build.BOOTLOADER) && !version.equals(""))
		{
			TableRow.LayoutParams textViewParams = new TableRow.LayoutParams(0,
					ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);

			TableLayout tableLayout = v.findViewById(R.id.tableOs);
			tableLayout.addView(Tools.getTableRowWithTextViews(getContext(),
					Tools.getTextViewLabel(getContext(), textViewParams, R.id.radio_version_label,
							R.string.baseband_version),
					Tools.getTextViewWithValue(getContext(), textViewParams, R.id.radioVersion,
							version)), 3,
					new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
							ViewGroup.LayoutParams.WRAP_CONTENT));
		}

	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	private void showBootloaderVersion(View v)
	{
		String bootloader = Build.BOOTLOADER;
		if (!bootloader.toUpperCase().equals("UNKNOWN"))
		{
			TableRow.LayoutParams textViewParams = new TableRow.LayoutParams(0,
					ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);

			TableLayout tableLayout = v.findViewById(R.id.tableOs);
			tableLayout.addView(Tools.getTableRowWithTextViews(getContext(),
					Tools.getTextViewLabel(getContext(), textViewParams, R.id.bootloader_label,
							R.string.bootloader),
					Tools.getTextViewWithValue(getContext(), textViewParams, R.id.bootloader,
							bootloader)), 3,
					new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
							ViewGroup.LayoutParams.WRAP_CONTENT));
		}
	}

	private void showAndroidSDK(TextView v)
	{
		SetData.setText(v, Build.VERSION.SDK_INT + "");
	}

	private void showAndroidVersion(TextView v)
	{
		SetData.setText(v, Build.VERSION.RELEASE);
	}

	protected void showFingerPrint(TextView v)
	{
		SetData.setText(v, Build.FINGERPRINT);
	}
}
