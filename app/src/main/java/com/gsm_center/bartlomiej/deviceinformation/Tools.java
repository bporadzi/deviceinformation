package com.gsm_center.bartlomiej.deviceinformation;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.widget.TableRow;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class Tools
{

	private final static int WIFI_CONNECTION = 1, MOBILE_CONNECTION = 0, NO_NETWORK = -1,
			SECURITY_EXCEPTION = -2;
	private final static int MIN_HEIGHT = 50, TEXT_SIZE = 14;


	public static double round(double value, int places)
	{
		try
		{
			if (places < 0) throw new IllegalArgumentException();

			BigDecimal bd = new BigDecimal(value);
			bd = bd.setScale(places, RoundingMode.HALF_UP);
			return bd.doubleValue();
		}
		catch (NumberFormatException e)
		{
			e.printStackTrace();
			return 0;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return 0;
		}
	}

	public static void enableWifi(Context context)
	{
		try
		{
			if (context == null) return;
			WifiManager wifiManager = ((WifiManager) context.getApplicationContext()
			                                                .getSystemService(
					                                                Context.WIFI_SERVICE));
			if (wifiManager == null) return;
			wifiManager.setWifiEnabled(true);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static TextView getTextViewLabel(Context context, TableRow.LayoutParams textViewParams,
			int labelId, int labelName)
	{
		TextView textViewLabel = new TextView(context);
		final float scale = context.getResources().getDisplayMetrics().density;
		int pixels = (int) (MIN_HEIGHT * scale + 0.5f);
		textViewLabel.setMinimumHeight(pixels);
		textViewLabel.setGravity(Gravity.CENTER);
		textViewLabel.setId(labelId);
		textViewLabel.setLayoutParams(textViewParams);
		textViewLabel.setText(labelName);
		textViewLabel.setTextSize(TEXT_SIZE);
		return textViewLabel;
	}

	public static TextView getTextViewLabel(Context context, TableRow.LayoutParams textViewParams,
			String labelName)
	{
		TextView textViewLabel = new TextView(context);
		final float scale = context.getResources().getDisplayMetrics().density;
		int pixels = (int) (MIN_HEIGHT * scale + 0.5f);
		textViewLabel.setMinimumHeight(pixels);
		textViewLabel.setGravity(Gravity.CENTER);
		textViewLabel.setLayoutParams(textViewParams);
		textViewLabel.setText(labelName);
		textViewLabel.setTextSize(TEXT_SIZE);
		return textViewLabel;
	}

	public static TextView getTextViewLabel(Context context, TableRow.LayoutParams textViewParams,
			int labelName)
	{
		TextView textViewLabel = new TextView(context);
		final float scale = context.getResources().getDisplayMetrics().density;
		int pixels = (int) (MIN_HEIGHT * scale + 0.5f);
		textViewLabel.setMinimumHeight(pixels);
		textViewLabel.setGravity(Gravity.CENTER);
		textViewLabel.setLayoutParams(textViewParams);
		textViewLabel.setText(labelName);
		textViewLabel.setTextSize(TEXT_SIZE);
		return textViewLabel;
	}

	public static TextView getTextViewWithValue(Context context,
			TableRow.LayoutParams textViewParams, int id, String textViewText)
	{
		TextView textView = new TextView(context);
		final float scale = context.getResources().getDisplayMetrics().density;
		int pixels = (int) (MIN_HEIGHT * scale + 0.5f);
		textView.setMinimumHeight(pixels);
		textView.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
		textView.setId(id);
		textView.setLayoutParams(textViewParams);
		textView.setText(textViewText);
		textView.setTextSize(TEXT_SIZE);
		return textView;
	}

	@Nullable
	private static String executeCommand(String command)
	{
		try
		{
			Process process = Runtime.getRuntime().exec(command);
			process.waitFor();
			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(process.getInputStream()));

			StringBuilder stringBuilder = new StringBuilder();
			String line;
			while ((line = bufferedReader.readLine()) != null)
			{
				stringBuilder.append(line);
			}
			return stringBuilder.toString();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

	@Nullable
	public static String readProp(String prop)
	{
		return Tools.executeCommand("getprop " + prop);
	}

	public static TextView getTextViewWithValue(Context context,
			TableRow.LayoutParams textViewParams, int id, int textViewText)
	{
		TextView textView = new TextView(context);
		final float scale = context.getResources().getDisplayMetrics().density;
		int pixels = (int) (MIN_HEIGHT * scale + 0.5f);
		textView.setMinimumHeight(pixels);
		textView.setGravity(Gravity.CENTER);
		textView.setLayoutParams(textViewParams);
		textView.setText(textViewText);
		textView.setId(id);
		textView.setTextSize(TEXT_SIZE);
		return textView;
	}

	public static TableRow getTableRowWithTextViews(Context context, TextView textViewLabel,
			TextView textView)
	{
		TableRow tableRow = new TableRow(context);
		tableRow.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
				TableRow.LayoutParams.WRAP_CONTENT, 1f));
		tableRow.setGravity(Gravity.CENTER);
		tableRow.setOrientation(TableRow.HORIZONTAL);
		tableRow.addView(textViewLabel);
		tableRow.addView(textView);
		return tableRow;
	}

	public static byte[] reverseArray(byte[] array)
	{
		byte tmp[] = new byte[array.length];

		for (int i = array.length - 1; i >= 0; i--)
		{
			tmp[array.length - i - 1] = array[i];
		}

		return tmp;
	}

	public static String formatCapactiy(long capacity)
	{
		String suffix = "";
		if (capacity >= 1024)
		{
			suffix = " KB";
			capacity /= 1024;
			if (capacity >= 1024)
			{
				suffix = " MB";
				double cap = capacity / 1024.0;
				if (cap >= 1024)
				{
					suffix = " GB";
					cap = cap / 1024;
				}
				return round(cap, 2) + suffix;
			}
		}
		return capacity + suffix;
	}

	public static int checkWiFi(Context context)
	{
		try
		{
			ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(
					Context.CONNECTIVITY_SERVICE);
			if (connManager == null) return SECURITY_EXCEPTION;
			NetworkInfo activeNetwork = connManager.getActiveNetworkInfo();
			if (activeNetwork != null)
			{
				if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
				{
					return WIFI_CONNECTION;
				}
				else
				{
					return MOBILE_CONNECTION;
				}
			}
			else
			{
				return NO_NETWORK;
			}
		}
		catch (SecurityException e)
		{
			return SECURITY_EXCEPTION;
		}
	}
}

