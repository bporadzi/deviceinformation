package com.gsm_center.bartlomiej.deviceinformation.sections;

import android.annotation.TargetApi;
import android.hardware.Camera;
import android.hardware.camera2.CameraCharacteristics;
import android.util.Size;

import com.gsm_center.bartlomiej.deviceinformation.Tools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
    Created by Bartłomiej Poradzisz
 */

@TargetApi (21)
public class CameraInfo
{

    @SuppressWarnings ("deprecated")
    public static double cameraResolution(int id)
    {
        try
        {
            Camera camera = Camera.open(id);
            List<Camera.Size> sizes = camera.getParameters().getSupportedPictureSizes();

            ArrayList<Integer> arrayListWidth = new ArrayList<>();
            ArrayList<Integer> arrayListHeight = new ArrayList<>();

            Camera.Size result;

            for (int i = 0; i < sizes.size(); i++)
            {
                result = sizes.get(i);
                arrayListWidth.add(result.width);
                arrayListHeight.add(result.height);
            }

            camera.release();

            return Tools.round(Collections.max(arrayListWidth) * Collections.max(arrayListHeight) / 1000000.0, 1);
        } catch (Exception e)
        {
            return -1;
        }
    }

    public static double cameraResolutionNewApi(CameraCharacteristics cameraCharacteristics)
    {
        Size dimension = cameraCharacteristics.get(CameraCharacteristics.SENSOR_INFO_PIXEL_ARRAY_SIZE);

        return (dimension != null) ? Tools.round(dimension.getWidth() * dimension.getHeight() / 1000000.0, 1) : 0.0;
    }
}
