package com.gsm_center.bartlomiej.deviceinformation.Dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.gsm_center.bartlomiej.deviceinformation.R;

/*
    Created by Bartłomiej on 23.08.2017 .
 */

public class HardresetDialog extends DialogFragment
{

	private final static int REQUEST_CODE_ENABLE_ADMIN = 10;
	private Activity activity;
	private LayoutInflater inflater;
	private DevicePolicyManager mDPM;
	private ComponentName mDeviceAdmin;

	public static HardresetDialog getInstance()
	{
		HardresetDialog hardresetDialog = new HardresetDialog();
		Bundle bundle = new Bundle();
		hardresetDialog.setArguments(bundle);
		return hardresetDialog;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		this.activity = getActivity();
		this.inflater = activity.getLayoutInflater();

		mDPM = (DevicePolicyManager) getActivity().getSystemService(Context.DEVICE_POLICY_SERVICE);
		mDeviceAdmin = new ComponentName(getActivity(), HardresetDialog.MyAdmin.class);
	}

	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		@SuppressLint("InflateParams") View v = inflater.inflate(R.layout.dialog_hard_reset, null);

		initDialogUi();

		final AlertDialog d = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle)
				.setTitle(getString(R.string.perform_hard_reset)).setCancelable(true)
				.setNegativeButton(activity.getString(R.string.cancel),
						(dialog, which) -> dismiss()).setPositiveButton(
						activity.getString(R.string.reset), (dialog, which) -> doSomething())
				.setView(v).create();

		d.setOnShowListener(dialog -> {
			Button b = d.getButton(DialogInterface.BUTTON_POSITIVE);
			b.setTextColor(getResources().getColor(R.color.colorPrimary));
		});

		return d;
	}

	private void doSomething()
	{
		try
		{
			if (!mDPM.isAdminActive(mDeviceAdmin))
			{
				Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
				intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mDeviceAdmin);
				intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
						"We need your confirmation: ");
				getActivity().startActivityForResult(intent, REQUEST_CODE_ENABLE_ADMIN);
			}
			else
			{
				new Handler().post(() -> mDPM.wipeData(0));
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private void initDialogUi()
	{

	}

	public static class MyAdmin extends DeviceAdminReceiver
	{

	}

}
