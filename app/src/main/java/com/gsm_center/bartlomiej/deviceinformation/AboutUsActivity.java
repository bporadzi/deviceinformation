package com.gsm_center.bartlomiej.deviceinformation;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class AboutUsActivity extends AppCompatActivity
{

	private static String ROOT_CHECKER_PACKAGE = "com.hard_root_checker.bartomiej.hardrootchecker";

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about_us);

		findViewById(R.id.card_view_frp_unlocker).setOnClickListener(clickListener);
		findViewById(R.id.card_view_hard_root_checker).setOnClickListener(clickListener);
	}

	private View.OnClickListener clickListener = v -> {
		switch (v.getId())
		{
			case R.id.card_view_frp_unlocker:
			{
				try
				{
					startActivity(new Intent(Intent.ACTION_VIEW,
							Uri.parse("http://www.hardreset.info/FRP-Unlocker/")));
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				return;
			}
			case R.id.card_view_hard_root_checker:
			{
				try
				{
					startActivity(new Intent(Intent.ACTION_VIEW,
							Uri.parse("market://details?id=" + ROOT_CHECKER_PACKAGE)));
				}
				catch (android.content.ActivityNotFoundException anfe)
				{
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(
							"https://play.google.com/store/apps/details?id="
									+ ROOT_CHECKER_PACKAGE)));
				}
				break;
			}
		}
	};

}
