package com.gsm_center.bartlomiej.deviceinformation.sections;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import java.util.UUID;

/*
    Created by Bartłomiej Poradzisz
 */

public class GeneralInfo
{

	@SuppressLint("HardwareIds")
	public static String getGeneralData(Context context, TelephonyManager telephonyManager,
			boolean hasPermission)
	{
		String generalData;

		generalData = "Manufacturer:\t" + Build.BRAND + "\n";
		generalData += "Model:\t" + Build.MODEL + "\n";
		generalData += "Serial:\t" + Build.SERIAL + "\n";
		generalData += "Android ID:\t" + getDeviceID(context) + "\n";
		generalData += "IMEI:\t" + getDeviceIMEI(telephonyManager, hasPermission) + "\n";
		generalData += "GSF ID:\t" + getGsfId(context) + "\n";
		generalData += "IMSI:\t" + getDeviceImsi(telephonyManager) + "\n";
		generalData += "UUID:\t" + getDeviceUUID(telephonyManager, hasPermission, context) + "\n";
		generalData += "SIM serial:\t" + getSimSerial(telephonyManager) + "\n";

		return generalData;
	}

	@SuppressLint("HardwareIds")
	public static String getDeviceID(Context context)
	{
		return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
	}

	public static String getCountryOfOrigin(String imei, String brand)
	{
		if (imei == null || brand == null) return "Not available";

		if (imei.length() < 15) return "Not available";

		String country;

		String numbers = "" + imei.charAt(6) + imei.charAt(7);

		if (brand.equals("SAMSUNG"))
		{
			switch (numbers)
			{
				case "05":
				{
					country = "Brazil/USA/Finland";
					break;
				}
				case "50":
				{
					country = "Brazil/USA/Finland";
					break;
				}
				case "08":
				{
					country = "Germany";
					break;
				}
				case "80":
				{
					country = "Germany";
					break;
				}
				case "01":
				{
					country = "Finland";
					break;
				}
				case "10":
				{
					country = "Finland";
					break;
				}
				case "03":
				{
					country = "China";
					break;
				}
				case "30":
				{
					country = "China";
					break;
				}
				case "04":
				{
					country = "China";
					break;
				}
				case "40":
				{
					country = "China";
					break;
				}
				case "06":
				{
					country = "Hong Kong/China/Mexico";
					break;
				}
				case "60":
				{
					country = "Hong Kong/China/Mexico";
					break;
				}
				case "13":
				{
					country = "Azerbaijan";
					break;
				}
				case "31":
				{
					country = "Azerbaijan";
					break;
				}
				case "02":
				{
					country = "UAE/India";
					break;
				}
				case "20":
				{
					country = "UAE/India";
					break;
				}
				default:
				{
					country = "Vietnam";
				}
			}
		}
		else
		{
			country = "Not available for " + brand + " phones";
		}
		return country;
	}

	@SuppressLint("HardwareIds")
	public static String getDeviceIMEI(TelephonyManager tm, boolean hasPermissions)
	{
		//TODO return secondary IMEI
		try
		{
			return (tm != null) ? tm.getDeviceId() : "Unable to read IMEI";
		}
		catch (SecurityException e)
		{
			if (!hasPermissions) { return "Permission denied"; }
			else { return "Unable to read IMEI"; }
		}
	}

	public static String getGsfId(Context context)
	{
		try
		{
			Uri URI = Uri.parse("content://com.google.android.gsf.gservices");
			String ID_KEY = "android_id";
			String params[] = {ID_KEY};
			Cursor c = context.getContentResolver().query(URI, null, null, params, null);
			if (c != null)
			{
				if (!c.moveToFirst() || c.getColumnCount() < 2)
				{
					c.close();
					return null;
				}
				c.close();
				return Long.toHexString(Long.parseLong(c.getString(1)));
			}
			else
			{
				return null;
			}

		}
		catch (Exception e)
		{
			return null;
		}
	}

	@SuppressLint("HardwareIds")
	public static String getSimSerial(TelephonyManager tm)
	{
		if (tm == null || detectSimState(tm) == TelephonyManager.SIM_STATE_ABSENT) return "No SIM";
		try
		{
			if (tm.getSimSerialNumber() != null) { return tm.getSimSerialNumber(); }
			else { return "Failed to resolve"; }
		}
		catch (SecurityException e)
		{
			return "Permission denied";
		}
	}

	private static int detectSimState(TelephonyManager tm)
	{
		return tm.getSimState();
	}

	public static String getDeviceImsi(TelephonyManager telephonyManager)
	{
		try
		{
			@SuppressLint("HardwareIds") String imsi = telephonyManager.getSubscriberId();
			return (imsi != null) ? imsi : "Failed to resolve";
		}
		catch (SecurityException e)
		{
			return "Permission denied";
		}
	}

	public static String getDeviceUUID(TelephonyManager tm, boolean hasPermission, Context context)
	{
		if (tm == null || context == null) return "Failed to resolve";
		String deviceId = getDeviceID(context);
		if (deviceId == null) return "Failed to resolve";
		String sImei = getDeviceIMEI(tm, hasPermission);
		if (sImei == null) return null;
		return new UUID(deviceId.hashCode(), ((long) sImei.hashCode() << 32)).toString();
	}
}
