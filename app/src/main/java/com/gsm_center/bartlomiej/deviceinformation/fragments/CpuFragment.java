package com.gsm_center.bartlomiej.deviceinformation.fragments;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.gsm_center.bartlomiej.deviceinformation.R;
import com.gsm_center.bartlomiej.deviceinformation.SetData;
import com.gsm_center.bartlomiej.deviceinformation.Tools;
import com.gsm_center.bartlomiej.deviceinformation.sections.CPUInfo;

import java.util.ArrayList;

import static com.gsm_center.bartlomiej.deviceinformation.Tools.round;

public class CpuFragment extends Fragment
{

	public Thread t;
	protected View v;
	int numberOfCores = -1;
	private ArrayList <TextView> textViews;
	private Handler handler;

	public CpuFragment()
	{
	}

	@Override
	public void onCreate(Bundle savedInstanceBundle)
	{
		super.onCreate(savedInstanceBundle);
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.fragment_cpu, container, false);
	}

	@Override
	public void onPause()
	{
		if (handler != null)
		{
			handler.removeCallbacksAndMessages(null);
			handler = null;
		}
		if (t != null)
		{
			try
			{
				t.interrupt();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			t = null;
		}

		v = null;

		super.onPause();
	}

	@Override
	public void onDestroy()
	{
		if (textViews != null)
		{
			textViews.clear();
			textViews = null;
		}

		super.onDestroy();
	}

	@Override
	public void onResume()
	{
		super.onResume();

		v = getView();
		handler = new Handler();

		if (v != null)
		{
			t = new Thread(this::refreshFrequencies);
			t.start();
		}
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);

		setCpuData();
	}


	@RequiresApi(api = Build.VERSION_CODES.M)
	private void setCpuAbi()
	{
		final TableLayout table = v.findViewById(R.id.cpuTable);

		new Thread(() -> {
			final String cpuAbi = CPUInfo.getCpuAbi();
			if (cpuAbi != null)
			{
				if (handler == null) return;
				handler.post(() -> {
					TableRow.LayoutParams textViewParams = new TableRow.LayoutParams(0,
							ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
					table.addView(Tools.getTableRowWithTextViews(getContext(),
							Tools.getTextViewLabel(getContext(), textViewParams, R.id.cpu_abi_label,
									R.string.cpu_abi_label),
							Tools.getTextViewWithValue(getContext(), textViewParams, R.id.cpu_abi,
									cpuAbi)), 3,
							new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
									ViewGroup.LayoutParams.WRAP_CONTENT));
				});
			}
		}).start();
	}

	private synchronized void refreshFrequencies()
	{
		try
		{
			while (true)
			{
				if (v != null)
				{
					if (handler != null)
					{
						handler.post(() -> {
							if (textViews != null)
							{
								for (int i = 0; i < textViews.size(); i++)
								{
									setFrequency(textViews.get(i), i);
								}
							}
						});
					}
				}
				try
				{
					Thread.sleep(500);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	private void setChipsetName()
	{
		final TableLayout table = v.findViewById(R.id.cpuTable);

		new Thread(() -> {
			final String chipsetName = Build.HARDWARE.toUpperCase();//CPUInfo.getChipset();

			if (chipsetName.equals("")) return;

			if (handler == null) return;
			handler.post(() -> {
				TableRow.LayoutParams textViewParams = new TableRow.LayoutParams(0,
						ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
				table.addView(Tools.getTableRowWithTextViews(getContext(),
						Tools.getTextViewLabel(getContext(), textViewParams, R.id.chipset_label,
								R.string.chipset_label),
						Tools.getTextViewWithValue(getContext(), textViewParams, R.id.chipset,
								chipsetName)), 0,
						new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
								ViewGroup.LayoutParams.WRAP_CONTENT));
			});
		}).start();
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	private void setCpuData()
	{
		textViews = new ArrayList <>();
		final TableRow.LayoutParams tableParams = new TableRow.LayoutParams(
				TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, 1f);

		v = getView();
		if (v != null)
		{
			setArchitecture(v);
			setNumberOfCores(v.findViewById(R.id.numberOfCores));
			setClockSpeed(v.findViewById(R.id.clockSpeed));
			setCpuFamily();
			setCpuAbi();
			setChipsetName();

			final TableLayout table = v.findViewById(R.id.cpuTable);

			for (int i = 0; i < numberOfCores; i++)
			{
				textViews.add(Tools.getTextViewWithValue(getContext(), tableParams, i,
						R.string.monitoring_default_value));
				table.addView(Tools.getTableRowWithTextViews(getContext(),
						Tools.getTextViewLabel(getContext(), tableParams,
								getResources().getString(R.string.core_label) + " " + i),
						textViews.get(textViews.size() - 1)));
			}
		}
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	private void setCpuFamily()
	{
		final TableLayout table = v.findViewById(R.id.cpuTable);

		new Thread(() -> {
			final String cpuFamily = CPUInfo.getCpuModel();

			if (cpuFamily != null && cpuFamily.matches(".*[a-z]*."))
			{
				if (handler == null) return;
				handler.post(() -> {
					TableRow.LayoutParams textViewParams = new TableRow.LayoutParams(0,
							ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
					table.addView(Tools.getTableRowWithTextViews(getContext(),
							Tools.getTextViewLabel(getContext(), textViewParams,
									R.id.cpu_family_label, R.string.cpu_family),
							Tools.getTextViewWithValue(getContext(), textViewParams, R.id.cpu_family,
									cpuFamily)), 1,
							new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
									ViewGroup.LayoutParams.WRAP_CONTENT));
				});
			}
		}).start();
	}

	private void setClockSpeed(TextView v)
	{
		int minFreq = CPUInfo.getCurrentFrequency(0, -1);
		double maxFreq = round(CPUInfo.getCurrentFrequency(0, 1) / 1000.0, 1);
		for (int i = 1; i < CPUInfo.getNumberOfCores(); i++)
		{
			double tmp = CPUInfo.getCurrentFrequency(i, -1);

			if (tmp < minFreq) minFreq = (int) tmp;
			tmp = round(CPUInfo.getCurrentFrequency(i, 1) / 1000.0, 1);

			if (tmp > maxFreq) maxFreq = tmp;
		}
		SetData.setText(v, minFreq + " MHz - " + maxFreq + " GHz");
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	private void setArchitecture(View v)
	{
		String arch = CPUInfo.getArchitecture();

		SetData.setText(v.findViewById(R.id.cpuArch), arch);

		if (arch.startsWith("arm") || arch.startsWith("aarch"))
		{
			String cpu = CPUInfo.getArmCPUName();

			if (cpu != null)
			{
				TableRow.LayoutParams textViewParams = new TableRow.LayoutParams(0,
						ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);

				TableLayout tableLayout = v.findViewById(R.id.cpuTable);
				tableLayout.addView(Tools.getTableRowWithTextViews(getContext(),
						Tools.getTextViewLabel(getContext(), textViewParams, R.id.cpu_name_label,
								R.string.cpu_name),
						Tools.getTextViewWithValue(getContext(), textViewParams, R.id.cpu_name,
								cpu)), 0,
						new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
								ViewGroup.LayoutParams.WRAP_CONTENT));

			}

		}
	}

	private void setFrequency(final TextView v, final int core)
	{
		int freq = CPUInfo.getCurrentFrequency(core, 0);

		if (freq > 0)
		{
			SetData.setText(v, freq + " MHz");
		}
		else
		{
			SetData.setText(v, "Offline");
		}
	}

	private void setNumberOfCores(TextView v)
	{
		numberOfCores = CPUInfo.getNumberOfCores();
		SetData.setText(v, numberOfCores + "");
	}
}
