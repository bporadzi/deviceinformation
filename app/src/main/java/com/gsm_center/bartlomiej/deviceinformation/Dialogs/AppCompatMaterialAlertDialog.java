package com.gsm_center.bartlomiej.deviceinformation.Dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import com.gsm_center.bartlomiej.deviceinformation.R;
import com.gsm_center.bartlomiej.deviceinformation.Tools;

/*
	Created by Bartłomiej Poradzisz
*/

public class AppCompatMaterialAlertDialog extends DialogFragment
{

	View v;
	private Activity activity;
	private LayoutInflater inflater;
	private CheckBox checkBoxShowAgain;

	public static AppCompatMaterialAlertDialog getInstance()
	{
		AppCompatMaterialAlertDialog appCompatMaterialAlertDialog
				= new AppCompatMaterialAlertDialog();
		Bundle bundle = new Bundle();
		appCompatMaterialAlertDialog.setArguments(bundle);
		return appCompatMaterialAlertDialog;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		this.activity = getActivity();
		this.inflater = activity.getLayoutInflater();
	}

	@SuppressLint("InflateParams")
	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		v = inflater.inflate(R.layout.dialog_wifi, null);

		initDialogUi(v);

		final AlertDialog d = new AlertDialog.Builder(activity, R.style.AppCompatAlertDialogStyle)
				.setTitle(getString(R.string.enable_wifi_title)).setCancelable(true)
				.setPositiveButton(activity.getString(R.string.enable_wifi), (dialog, which) -> {
					try
					{
						doSomething(true);
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
					dismiss();
				}).setNegativeButton(activity.getString(R.string.keep_disabled),
						(dialog, which) -> {
							try
							{
								doSomething(false);
							}
							catch (Exception e)
							{
								e.printStackTrace();
							}
							dismiss();
						}).setView(v).create();

		d.setOnShowListener(dialog -> {
			Button b = d.getButton(DialogInterface.BUTTON_POSITIVE);
			b.setTextColor(getResources().getColor(R.color.colorPrimary));
		});

		return d;
	}

	private void doSomething(final boolean accepted)
	{
		new Thread(() -> {
			try
			{
				saveUserChoice(accepted, checkBoxShowAgain.isChecked());
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}).start();

		if (accepted)
		{
			new Handler().post(() -> Tools.enableWifi(getActivity().getApplicationContext()));
		}
	}

	private void saveUserChoice(boolean accept, boolean value)
	{
		SharedPreferences sharedPreferences = this.getActivity().getApplicationContext()
		                                          .getSharedPreferences("values",
				                                          Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean("never_ask", value);
		editor.putBoolean("enable_wifi_automatically", accept);
		editor.apply();
	}

	private void initDialogUi(View root)
	{
		checkBoxShowAgain = root.findViewById(R.id.neverShowAgain);
	}
}
