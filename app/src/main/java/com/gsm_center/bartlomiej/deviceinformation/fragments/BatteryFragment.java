package com.gsm_center.bartlomiej.deviceinformation.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.gsm_center.bartlomiej.deviceinformation.R;
import com.gsm_center.bartlomiej.deviceinformation.SetData;
import com.gsm_center.bartlomiej.deviceinformation.Tools;
import com.gsm_center.bartlomiej.deviceinformation.sections.BatteryInfo;

import java.util.Objects;

public class BatteryFragment extends Fragment
{

	boolean batteryLow = false;
	Thread t;
	View v;
	private Handler handler;

	public BatteryFragment()
	{
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		return inflater.inflate(R.layout.fragment_battery, container, false);
	}

	@Override
	public void onResume()
	{
		super.onResume();

		v = getView();

		handler = new Handler();
		t = new Thread(this::refreshVoltage);
		t.start();
	}

	@Override
	public void onPause()
	{
		if (handler != null)
		{
			handler.removeCallbacksAndMessages(null);
			handler = null;
		}
		if (t != null)
		{
			try
			{
				t.interrupt();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			t = null;
		}

		v = null;

		super.onPause();
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{

		super.onActivityCreated(savedInstanceState);
		v = getView();
		if (v != null)
		{
			setBatteryInfo();
			setBatteryCondition(v.findViewById(R.id.batteryHealth));
			setBatteryTechnology();
		}
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	protected void setBatteryCondition(TextView v)
	{
		SetData.setText(v, BatteryInfo.getBatteryHealth(Objects.requireNonNull(this.getContext())));
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	protected void refreshVoltage()
	{
		while (true)
		{
			try
			{
				if (handler != null)
				{
					handler.post(() -> {
						if (v != null) setBatteryVoltage(v.findViewById(R.id.batteryVoltage));
					});
				}

				Thread.sleep(1500);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	protected void setBatteryInfo()
	{
		if (v != null)
		{
			setBatteryStatus(v.findViewById(R.id.batteryStatus));
			setBatteryLevel(v.findViewById(R.id.batteryLevel));
		}
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	protected void setBatteryStatus(TextView v)
	{
		if (!batteryLow)
		{
			SetData.setText(v,
					BatteryInfo.getBatteryStatus(Objects.requireNonNull(this.getContext())));
		}
		else { SetData.setText(v, "Battery low!"); }
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	protected void setBatteryLevel(TextView v)
	{
		int level = BatteryInfo.getBatteryLevel(Objects.requireNonNull(this.getContext()));
		if (level >= 20) batteryLow = false;

		if (level == -1)
		{
			batteryLow = false;
			SetData.setText(v, "Failed to resolve");
			return;
		}

		SetData.setText(v, level + "%");
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	protected void setBatteryVoltage(TextView v)
	{
		SetData.setText(v,
				BatteryInfo.getBatteryVoltage(Objects.requireNonNull(this.getContext())));
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	protected void setBatteryTechnology()
	{
		Context context = getContext();
		if (context == null) return;

		String technology = BatteryInfo.getBatteryTechnology(context);

		if (!technology.equals(""))
		{
			TableRow.LayoutParams textViewParams = new TableRow.LayoutParams(0,
					ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);

			TableLayout tableLayout = v.findViewById(R.id.tableBattery);
			tableLayout.addView(Tools.getTableRowWithTextViews(getContext(),
					Tools.getTextViewLabel(getContext(), textViewParams,
							R.id.battery_technology_label, R.string.battery_technology),
					Tools.getTextViewWithValue(getContext(), textViewParams, R.id.battery_technology,
							technology)), 3,
					new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
							ViewGroup.LayoutParams.WRAP_CONTENT));
		}
	}

	public BroadcastReceiver getBatteryReceiver()
	{
		return new BroadcastReceiver()
		{

			@RequiresApi(api = Build.VERSION_CODES.M)
			@Override
			public void onReceive(Context context, Intent intent)
			{
				if (intent == null) return;
				String intentType = intent.getType();
				try
				{
					if (intentType == null) throw new Exception();
					if (intentType.equals(Intent.ACTION_BATTERY_LOW)) batteryLow = true;
					if (intentType.equals(Intent.ACTION_BATTERY_OKAY)) batteryLow = false;
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
				finally
				{
					setBatteryInfo();
				}
			}
		};
	}
}
