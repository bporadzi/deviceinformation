package com.gsm_center.bartlomiej.deviceinformation.sections;

import android.support.annotation.Nullable;

import com.gsm_center.bartlomiej.deviceinformation.Tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Pattern;

import static com.gsm_center.bartlomiej.deviceinformation.Tools.round;

/**
 * Created by Bartłomiej Poradzisz
 */

public class CPUInfo
{

	private static final int INSERTION_POINT = 27;

	public static String getCpuData()
	{
		String cpuData;
		cpuData = "Architecture:\t" + getArchitecture() + "\n";
		cpuData += "Number of cores:\t" + getNumberOfCores() + "\n";
		int minFreq = CPUInfo.getCurrentFrequency(0, -1);
		double maxFreq = round(CPUInfo.getCurrentFrequency(0, 1) / 1000.0, 1);
		cpuData += "Clock speed:\t" + minFreq + " MHz - " + maxFreq + " GHz" + "\n";
		return cpuData;
	}

	@Nullable
	public static String getCpuName()
	{
		String info = null;

		try
		{
			info = new Scanner(new File("/proc/cpuinfo")).useDelimiter("\\Z").next();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}

		if (info == null) return null;
		int start = info.indexOf("model name") + "model name".length() + 3;
		int end = info.indexOf(")") + 1;
		if (end <= start) return null;
		String res = info.substring(start, end);
		return res.length() > 0 ? res : null;
	}

	public static String getCpuModel()
	{
		return Tools.readProp("dalvik.vm.isa.arm.variant");
	}

	public static String getCpuAbi()
	{
		return Tools.readProp("ro.product.cpu.abi");
	}

	@Nullable
	public static String getArmCPUName()
	{
		try
		{
			FileReader fr = new FileReader("/proc/cpuinfo");
			BufferedReader br = new BufferedReader(fr);
			String text = br.readLine();
			br.close();
			String[] array = text.split(":\\s+", 2);
			if (array.length >= 2)
			{
				return (!array[1].equals("0")) ? array[1] : getCpuName();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public static String getArchitecture()
	{
		return System.getProperty("os.arch");
	}

	private static String getCurFrequencyFilePath(int whichCpuCore, String whatToRead)
	{
		StringBuilder filePath = new StringBuilder(
				"/sys/devices/system/cpu/cpu/cpufreq/" + whatToRead);
		filePath.insert(INSERTION_POINT, whichCpuCore);
		return filePath.toString();
	}

	//TODO check max && min freq for different cores

	public static int getCurrentFrequency(int whichCpuCore, int choice)
	{
		int curFrequency = -1;
		String cpuCoreCurFreqFilePath = "";
		switch (choice)
		{
			case -1:
			{
				cpuCoreCurFreqFilePath = getCurFrequencyFilePath(whichCpuCore, "cpuinfo_min_freq");
				break;
			}
			case 0:
			{
				cpuCoreCurFreqFilePath = getCurFrequencyFilePath(whichCpuCore, "scaling_cur_freq");
				break;
			}
			case 1:
			{
				cpuCoreCurFreqFilePath = getCurFrequencyFilePath(whichCpuCore, "cpuinfo_max_freq");
				break;
			}
		}
		if (new File(cpuCoreCurFreqFilePath).exists())
		{
			try
			{
				BufferedReader br = new BufferedReader(
						new FileReader(new File(cpuCoreCurFreqFilePath)));
				String aLine;
				while ((aLine = br.readLine()) != null)
				{
					try
					{
						curFrequency = Integer.parseInt(aLine);
					}
					catch (NumberFormatException e)
					{
						e.printStackTrace();
					}
				}
				br.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		return curFrequency / 1000;
	}

	public static int getNumberOfCores()
	{
		class CpuFilter implements FileFilter
		{

			@Override
			public boolean accept(File pathname)
			{
				return Pattern.matches("cpu[0-9]+", pathname.getName());
			}
		}

		try
		{
			File dir = new File("/sys/devices/system/cpu/");
			File[] files = dir.listFiles(new CpuFilter());
			return files.length;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return -1;
		}
	}

    /*
    public static String getX86CPUName()
    {
        String aLine = "Intel";
        if (new File("/proc/cpuinfo").exists())
        {
            try
            {
                BufferedReader br = new BufferedReader(new FileReader(new File("/proc/cpuinfo")));
                String strArray[] = new String[2];
                while ((aLine = br.readLine()) != null)
                {
                    if (aLine.contains("model name"))
                    {
                        br.close();
                        strArray = aLine.split(":", 2);
                        aLine = strArray[1];
                    }
                }
                if (br != null)
                {
                    br.close();
                }
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        return aLine;
    }
    */
    /*
    public static String getMIPSCPUName()
    {
        String aLine = "MIPS";
        if (new File("/proc/cpuinfo").exists())
        {
            try
            {
                BufferedReader br = new BufferedReader(new FileReader(new File("/proc/cpuinfo")));
                String strArray[] = new String[2];
                while ((aLine = br.readLine()) != null)
                {
                    if (aLine.contains("cpu model"))
                    {
                        br.close();
                        strArray = aLine.split(":", 2);
                        aLine = strArray[1];
                    }
                }
                if (br != null)
                {
                    br.close();
                }
            } catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        return aLine;
    }*/
}
