package com.gsm_center.bartlomiej.deviceinformation.sections;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.camera2.CameraManager;
import android.os.Build;

/**
 * Created by Bartłomiej Poradzisz
 */

public class CameraInf
{
    private Context context;

    public CameraInf(Context context)
    {
        this.context = context;
        gatherData();
    }

    public void gatherData()
    {
        new Thread(() -> {
            SharedPreferences sharedPreferences = context.getSharedPreferences("values", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            if (!context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA))
                return;

            System.out.println("Cameras detected");

            if (Build.VERSION.SDK_INT >= 21)
            {
                CameraManager cameraManager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);

                try
                {
                    if (cameraManager == null) throw new Exception();

                    String[] cameraIds = cameraManager.getCameraIdList();
                    editor.putInt("number_of_cameras", cameraIds.length);

                    for (int i = 0; i < cameraIds.length; i++)
                    {
                        String cameraNumber = "camera" + i;
                        editor.putFloat(cameraNumber, (float) CameraInfo.cameraResolutionNewApi(cameraManager.getCameraCharacteristics(cameraIds[i])));
                    }
                } catch (Exception e)
                {
                    return;
                }
            } else
            {
                int numberOfCameras = Camera.getNumberOfCameras();
                editor.putInt("number_of_cameras", numberOfCameras);
                for (int i = 0; i < numberOfCameras; i++)
                {
                    String tmp = "camera" + i;
                    editor.putFloat(tmp, (float) CameraInfo.cameraResolution(i));
                }

            }
            editor.apply();
        }).start();
    }
}
