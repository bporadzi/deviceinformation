package com.gsm_center.bartlomiej.deviceinformation;

import android.text.SpannableStringBuilder;
import android.widget.TextView;

/*
    Created by Bartłomiej Poradzisz
*/
public class SetData
{

	public static void setText(TextView fieldName, String text)
	{
		try
		{
			if (text != null && !text.equals("") && !text.equals("null")) fieldName.setText(text);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void setText(TextView fieldName, SpannableStringBuilder text)
	{

		if (text != null && text.length() != 0 && !text.toString().equals("null"))
		{ fieldName.setText(text); }
	}

	public static void setMacAddress(TextView textViewMac, String mac)
	{
		try
		{
			if (mac != null)
			{
				textViewMac.setText(mac);
			}
			else
			{
				textViewMac.setText(R.string.noNetworkCard);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
