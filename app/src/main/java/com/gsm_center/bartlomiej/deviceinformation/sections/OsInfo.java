package com.gsm_center.bartlomiej.deviceinformation.sections;

import android.annotation.SuppressLint;
import android.os.Build;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Method;

public class OsInfo
{

	public static String getOsData()
	{
		String osData;

		osData = "Android version:\t" + Build.VERSION.RELEASE + "\n";
		osData += "SDK:\t" + Build.VERSION.SDK_INT + "\n";
		osData += "Code name:\t" + getCodename(Build.VERSION.SDK_INT + "") + "\n";
		osData += "Baseband version:\t" + Build.BOOTLOADER + "\n";
		osData += "Build ID:\t" + Build.DISPLAY + "\n";
		osData += "Fingerprint:\t" + Build.FINGERPRINT + "\n";
		osData += "Kernel:\t" + getKernelInfo() + "\n";
		osData += "Software release date:\t" + getKernelBuildDate() + "\n";
		osData += "JVM version:\t" + getJavaVersion() + "\n";
		osData += "Android runtime:\t" + getJavaRuntime(getJavaVersion()) + "\n";
		osData += "Root:\t" + (isRooted() ? "Yes" + "\n" : "No" + "\n");
		return osData;
	}

	public static String getJavaRuntime(String version)
	{
		return (version.startsWith("2")) ? "ART" : "Dalvik";
	}

	public static String getCodename(String apiVersion)
	{
		switch (apiVersion)
		{
			case "15":
			{
				return "Ice Cream Sandwich";
			}
			case "16":
			{
				return "Jelly Bean";
			}
			case "17":
			{
				return "Jelly Bean";
			}
			case "18":
			{
				return "Jelly Bean";
			}
			case "19":
			{
				return "KitKat";
			}
			case "21":
			{
				return "Lollipop";
			}
			case "22":
			{
				return "Lollipop";
			}
			case "23":
			{
				return "Marshmallow";
			}
			case "24":
			{
				return "Nougat";
			}
			case "25":
			{
				return "Nougat";
			}
			case "26":
			{
				return "Oreo";
			}
			case "27":
			{
				return "Oreo";
			}
			default:
			{
				return "Failed to resolve";
			}
		}
	}

	public static String getSecurityPatch()
	{
		if (Build.VERSION.SDK_INT >= 23)
		{
			return Build.VERSION.SECURITY_PATCH;
		}
		else
		{
			return "Not available for API lower than 23";
		}
	}

	public static boolean isRooted()
	{
		return findBinary();
	}

	private static boolean findBinary()
	{
		boolean found = false;

		String[] places = {"/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/",
				"/data/local/bin/", "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/"
		};
		for (String where : places)
		{
			if (new File(where + "su").exists())
			{
				found = true;
				break;
			}
		}

		return found;
	}

	public static String getSamsungCSC()
	{
		File file = new File("/system/csc/sales_code.dat");

		StringBuilder code = new StringBuilder();

		try
		{
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;

			while ((line = br.readLine()) != null)
			{
				code.append(line);
				code.append('\n');
			}
			br.close();
		}
		catch (IOException e)
		{
			return null;
		}

		return code.toString();
	}

	public static String getJavaVersion()
	{
		return System.getProperty("java.vm.version");
	}

	public static String getKernelBuildDate()
	{
		StringBuilder tmp = new StringBuilder();

		String path = "/proc/version";

		FileReader fstream;

		try
		{
			fstream = new FileReader(path);
		}
		catch (FileNotFoundException e)
		{
			return null;
		}
		BufferedReader in = new BufferedReader(fstream, 500);
		String line;

		try
		{
			while ((line = in.readLine()) != null)
			{
				tmp.append(line);
			}
		}
		catch (IOException e)
		{
			return null;
		}

		int startString = tmp.length() - 24;

		String month = tmp.substring(startString, startString + 4) + " ";

		startString += 4;
		String day = tmp.substring(startString, startString + 2) + " ";

		startString += 3;
		String hour = tmp.substring(startString, startString + 8);

		startString += 8;
		String timeZone = tmp.substring(startString, startString + 4);

		startString += 5;
		String year = tmp.substring(startString, startString + 4);

		return day + month + year + " " + hour + timeZone;
	}

	public static String getKernelInfo()
	{
		return System.getProperty("os.version");
	}

	public static String getSamsungSerialNumber()
	{
		String serial = null;
		try
		{
			@SuppressLint("PrivateApi") Class <?> c = Class.forName("android.os.SystemProperties");
			Method get = c.getMethod("get", String.class, String.class);
			serial = (String) get.invoke(c, "ril.serialnumber", "unknown");
		}
		catch (Exception ignored)
		{
		}
		return serial;
	}
}
